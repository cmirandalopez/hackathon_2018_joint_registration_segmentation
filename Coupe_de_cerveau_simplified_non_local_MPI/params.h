#ifndef PARAMS_H_
#define PARAMS_H_

/*Nombre maximal d'itÃ©rations*/
const int IterMax = 500;


const int n_max=300;


/*****Parameter related to the Decoupled Problem*****/
const double epsilon1=0.1;
const double epsilon2=80000.0; /*appearing in the splitting part*/
const double c=10.0;//parameter appearing in the edge detector function
const double lambdal2=1.5;
const double lambdanl=1;/*L2 data fidelity term*/

/*****Coefficients related to the material*****/
const double coeff_lame_lambda=10.0;
const double coeff_lame_mu=1000.0;
const double dt=0.01; //pas de temps

//double h=1.0;
const double eps=1.0;
const double tol=0.075;
const double rho=0.5;
const double theta=1.0;

#endif
