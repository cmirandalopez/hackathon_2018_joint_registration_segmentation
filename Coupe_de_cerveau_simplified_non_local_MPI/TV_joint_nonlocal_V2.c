#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <omp.h>
#include <time.h>
#include <unistd.h>
#include <mpi.h>

#ifdef MKL_BLAS
#include <mkl_cblas.h>
#include <mkl_lapack.h>
#else
#include <cblas.h>
#endif

#define epsilon 10e-10
#define egal(a,b) a-b<epsilon
#define max(a,b) ((a)>(b)?(a):(b)) 
#define min(a,b) ((a)>(b)?(b):(a)) 
//#include "dmumps_c.h"
#define JOB_INIT -1
#define JOB_END -2
#define USE_COMM_WORLD -987654
//#define ICNTL(I) icntl[(I)-1] /* macro s.t. indices match documentation */
#define M_PI 3.14159265359
#define DOSSIER ""
#define YES 0
#define NO 1
#define SWAP(a,b,tmp) tmp=a; a=b; b=tmp
#define EXP_A (1048576/0.69314718055994530942)
#define EXP_C 60801
#define DEXP(y) (_eco.n.i = EXP_A*(y) + (1072693248 - EXP_C), _eco.d)
#define EXP(y) (double)DEXP(y)

#include "functions.h"
#include "mpi_params.h"
#include "dimens.h"

int M_R=180;//nb REEL  lignes de l'image
int N_R=150;//nb REEL colonnes de l'image


/*Coefficient de rÃ©gularisation dans la phase d'interpolation*/
double theta_lip=0.1;

/* rang dans le communicateur initial */
int rang;
/* nombre de processus */
int nb_procs;



  /**************************************************************************************************/
  /*********************************INITIALISATION***************************************************/
  /**************************************************************************************************/
  void initialisation_mpi(int argc, char* argv[]) {
  /* Initialisation de MPI */
  MPI_Init(&argc, &argv);

  /* Savoir quel processus je suis */
  MPI_Comm_rank(MPI_COMM_WORLD, &rang);

  /* Connaitre le nombre total de processus */
  MPI_Comm_size(MPI_COMM_WORLD, &nb_procs);
 }

  /**************************************************************************************************/
  /*********************************FINALISATION***************************************************/
  /**************************************************************************************************/
  
  void finalisation_mpi() {
  /* Desactivation de MPI */
  MPI_Finalize();
  }


  /**************************************************************************************************/
  /*********************************CREATION DOMAINE*************************************************/
  /**************************************************************************************************/
  
  void domaine(MPI_Comm comm2d,int rang,int * coords,int ntx,int nty,int * dims,int * tab_bounds,int * periods,int reorganisation,int nb_procs) {
  

 
 
  int nx,ny,positionx,positiony,restex,restey;
  int sx,ex,sy,ey; 
  
  /* ConnaÃ®tre mes coordonnees dans la topologie */
  MPI_Cart_coords(comm2d,rang,ndims,coords);
  
  
  /* Calcul pour chaque processus de ses indices de debut et de fin suivant x */
  
  /*Nombre de points dans la direction x*/

  nx=ntx/dims[1];
  restex=ntx % dims[1];
  positionx=coords[1];
  sx=1+positionx*nx+min(restex,positionx);/*Indice de depart dans la direction des x*/
  if(positionx<restex){
  nx=nx+1;
  }
 
 
  ex=sx+nx-1; /*Indice de fin dans la direction des x*/
  ex=min(ex,ntx+1);/*correction si besoin pour le dernier bloc*/

  
  /**********************************************************/
  /*Pour renumeroter selon la direction opposee a dims[1]   */
  /**********************************************************/


  
  sx=ntx-ex+1;
  ex=sx+nx-1;
 
  /*Nombre de points dans la direction y*/

  ny=nty/dims[0];
  restey=nty % dims[0];
  positiony=coords[0];
  sy=1+positiony*ny+min(restey,positiony);/*Indice de depart dans la direction des y*/
  if(positiony<restey){
  ny=ny+1;
  }

  ey=sy+ny-1;/*Indice de fin*/
  ey=min(ey,nty+1);
 /*correction si besoin pour le dernier bloc*/

  tab_bounds[0]=sx;
  tab_bounds[1]=ex;
  tab_bounds[2]=sy;
  tab_bounds[3]=ey;

  
  }

  /**************************************************************************************************/
  /*********************************VOISINAGE********************************************************/
  /**************************************************************************************************/
  

  void voisinage(MPI_Comm comm2d,int * voisin, int * voisin_diagonale,int * coords, int * dims) {


  int coords_NE[ndims];
  int coords_ES[ndims];
  int coords_SO[ndims];
  int coords_ON[ndims];

  /* Recherche des voisins Nord et Sud */
  MPI_Cart_shift(comm2d, 0, 1, &(voisin[W]), &(voisin[E]));

  /* Recherche des voisins Ouest et Est */
  MPI_Cart_shift(comm2d, 1, 1, &(voisin[S]), &(voisin[North]));

  /****Recherche des voisins diagonaux****/
  /****Voisin nord-est***/
  if(coords[0]+1<=dims[0]-1&&coords[1]+1<=dims[1]-1){
  coords_NE[0]=coords[0]+1;
  coords_NE[1]=coords[1]+1;
  MPI_Cart_rank(comm2d,coords_NE,&(voisin_diagonale[NE]));  
  }
  else
  {
  voisin_diagonale[NE]=MPI_PROC_NULL;
  }

  /****Voisin est-sud***/
  if(coords[0]+1<=dims[0]-1&&coords[1]-1>=0){
  coords_ES[0]=coords[0]+1;
  coords_ES[1]=coords[1]-1;
  MPI_Cart_rank(comm2d,coords_ES,&(voisin_diagonale[ES]));  
  }
  else
  {
  voisin_diagonale[ES]=MPI_PROC_NULL;
  }

  /****Voisin sud-ouest***/

  if(coords[0]-1>=0&&coords[1]-1>=0){
  coords_SO[0]=coords[0]-1;
  coords_SO[1]=coords[1]-1;
  MPI_Cart_rank(comm2d,coords_SO,&(voisin_diagonale[SO]));  
  }
  else
  {
  voisin_diagonale[SO]=MPI_PROC_NULL;
  }

  /****Voisin ouest-nord***/

  if(coords[0]-1>=0&&coords[1]+1<=dims[1]-1){
  coords_ON[0]=coords[0]-1;
  coords_ON[1]=coords[1]+1;
  MPI_Cart_rank(comm2d,coords_ON,&(voisin_diagonale[ON]));  
  }
  else
  {
  voisin_diagonale[ON]=MPI_PROC_NULL;
  }
  
} 

 void ecrire_mpi(double *v2_local_vect,int ntx,int nty,int * tab_bounds,MPI_Comm comm2d,char * nom_fichier){

  int code;
  MPI_File descripteur;
  int profil_tab[ndims], profil_sous_tab[ndims], coord_debut[ndims];
  MPI_Datatype type_sous_tab, type_sous_tab_vue;
  int profil_tab_vue[ndims], profil_sous_tab_vue[ndims], coord_debut_vue[ndims];
  MPI_Offset deplacement_initial;
  MPI_Status statut;

  /* Ouverture du fichier "final_u.dat" en Ã©criture */
  code = MPI_File_open(comm2d,nom_fichier, MPI_MODE_WRONLY+MPI_MODE_CREATE, 
		MPI_INFO_NULL, &descripteur);

 /* Test pour savoir si ouverture du fichier est correcte */
  if (code != MPI_SUCCESS) {
    fprintf(stderr, "ATTENTION erreur lors ouverture du fichier");
    MPI_Abort(comm2d, 2);
  }

  /* Creation du type derive type_sous_tab qui definit la matrice u
   * sans les cellules fantomes */
  profil_tab[0] = tab_bounds[1]-tab_bounds[0]+3;
  profil_tab[1] = tab_bounds[3]-tab_bounds[2]+3;

  /* Profil du sous tableau */
  profil_sous_tab[0] =tab_bounds[1]-tab_bounds[0]+1;
  profil_sous_tab[1] =tab_bounds[3]-tab_bounds[2]+1;

  /* Coordonnees de depart du sous tableau */
  coord_debut[0] = 1;
  coord_debut[1] = 1;

  /* Creation du type_derive type_sous_tab */
  MPI_Type_create_subarray(ndims, profil_tab, profil_sous_tab, coord_debut, 
			    MPI_ORDER_C, MPI_DOUBLE, &type_sous_tab);

  /* Validation du type_derive type_sous_tab */
  MPI_Type_commit(&type_sous_tab);

  /* Creation du type type_sous_tab_vue  pour la vue sur le fichier */
  /* Profil du tableau global */
  profil_tab_vue[0] = ntx;
  profil_tab_vue[1] = nty;

  /* Profil du sous tableau */
  profil_sous_tab_vue[0] = tab_bounds[1]-tab_bounds[0]+1;
  profil_sous_tab_vue[1] = tab_bounds[3]-tab_bounds[2]+1;

  /* Coordonnees de depart du sous tableau */
  coord_debut_vue[0] = tab_bounds[0]-1;
  coord_debut_vue[1] = tab_bounds[2]-1;

  /* Creation du type_derive type_sous_tab_vue */
  MPI_Type_create_subarray(ndims, profil_tab_vue, profil_sous_tab_vue, coord_debut_vue, 
			   MPI_ORDER_C, MPI_DOUBLE, &type_sous_tab_vue);

  /* Validation du type_derive type_sous_tab_vue */
  MPI_Type_commit(&type_sous_tab_vue);

  /* DÃ©finition de la vue sur le fichier a partir du debut */
  deplacement_initial = 0;
  MPI_File_set_view(descripteur, deplacement_initial, MPI_DOUBLE, 
		    type_sous_tab_vue, "native", MPI_INFO_NULL);

  /* Ecriture du tableau u par tous les processus avec la vue */
  MPI_File_write_all(descripteur, v2_local_vect, 1, type_sous_tab, &statut);

  /* Fermeture du fichier */
  MPI_File_close(&descripteur);
}


void ecrire_fichier(FILE* final, char* nom_fichier, double** mat, int li, int col){

	int i,j;
	char chemin[1024];
	strcpy(chemin, (const char *) DOSSIER);
	strcat(chemin, "out_ChanVese/");

	strcat(chemin,nom_fichier);
	final=fopen(chemin,"w");

	if(final!=NULL){
		for(i=0;i<li;i++){
			for(j=0;j<col;j++){
				fprintf(final,"%lf\t",mat[i][j]);
			}
			fprintf(final,"\n");
		}
		fclose(final);
	}
	else printf("Erreur dans l'Ã©criture du fichier %s  \n", nom_fichier);
}

int main(int argc, char *argv[]) {
  
/* coordonnÃ©es dans la grille */
  int coords[ndims];
  /* tableau des dimensions dans la grille */
  int dims[ndims];
  /* communicateur topologie cartÃ©sienne */
  MPI_Comm comm2d;
  int periods[ndims];
  const int reorganisation=faux;
  /* tableau contenant les voisins du sous-domaine courant (haut,bas,gauche,droite)*/
  int voisin[NB_VOISINS];
  /* tableau contenant les voisins du sous-domaine courant dans les directions diagonales*/
  int voisin_diagonale[NB_VOISINS_DIAGONAUX];
 

  
  /*nombre total de points intÃ©rieurs dans la direction x et la direction y*/
  int ntx, nty;

  
  int it;
  double t1, t2;

  int NbNeigh=10;
  int iNbNeigh;
  iNbNeigh=NbNeigh;
  int im=5;
  int im2=(im-1)/2;
  int iw=11;
  int iw2=(iw-1)/2;
  double h=0.25*0.25;
  int IncludeCloseNeigh=YES;
 



 
  /*
        \
 ------- y                      coords[1]/dims[1]
 |      /                     /|\
 |                             |
 |                             |  
\ /                            |
 x                             |       \
                               --------- coords[0]/dims[0]
                                       /
*/

  initialisation_mpi(argc,argv);

  /* Creation de la topologie cartesienne 2D */
  /*Le nombre de points dans la direction x correspond au nombre de lignes-2 (uniquement les points intÃ©rieurs) */
  
  ntx=M_R-2;
  nty=N_R-2; 
  
  /* ConnaÃ®tre le nombre de processus selon x et le nombre de processus
     selon y en fonction du nombre total de processus */
  
  dims[0] = dims[1] = 0;
  MPI_Dims_create(nb_procs,ndims,dims);
  
  /* Creation de la grille de processus 2D sans periodicite */
  
  periods[0] = periods[1] = faux;
  MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, reorganisation, &comm2d);
  MPI_Comm_rank(comm2d,&rang);
  
  /*if(rang == 0) {
    printf("Execution code TV_joint avec %d processus MPI\n"
	   "Taille du domaine : ntx=%d nty=%d\n"
	   "Dimension de la topologie : %d suivant y (colonnes), %d suivant x (lignes)\n"
	   "-----------------------------------------\n", 
	   nb_procs, ntx, nty, dims[0], dims[1]);
  }*/

  int tab_bounds[4]; /*sx,ex,sy,ey*/
  /* Determinination des indices de chaque sous domaine */

  domaine(comm2d,rang,coords,ntx,nty,dims,tab_bounds,periods,reorganisation,nb_procs);
  /*printf("Je suis le rang %d\n"
	   "Ma coord 0 dans la direction des colonnes est: coord0=%d\n"
	   "Ma coord 1 dans la direction opposee a la direction des lignes est: coord1=%d\n"
	   "-----------------------------------------\n", 
	   rang, coords[0], coords[1]);*/
  /*if(rang==0){
  printf("Je suis le rang %d \n"
  "coordonnee en ligne du coin superieur gauche sx=%d\n"
  "coordonnee en colonne du coin superieur gauche sy=%d\n"
  "coordonnee en ligne du coin inferieur droit ex=%d\n"
  "coordonnee en colonne du coin inferieur droit ey=%d\n"
  "-----------------------------------------\n", 
	   rang,tab_bounds[0],tab_bounds[2],tab_bounds[1],tab_bounds[3]);
  }
*/
  

  
 
  /* Recherche de ses 4 voisins pour chaque processus */
  voisinage(comm2d,voisin,voisin_diagonale,coords,dims);
  /*if(rang==0){
  printf("Je suis le rang %d \n"
  "voisin nord est=%d\n"
  "voisin east south=%d\n"
  "voisin south west=%d\n"
  "voisin west north =%d\n"
  "-----------------------------------------\n", 
	   rang,voisin_diagonale[NE],voisin_diagonale[ES],voisin_diagonale[SO],voisin_diagonale[ON]);
  }*/
 
  int code;
  MPI_File descripteur; 
  MPI_Offset deplacement_initial;
  MPI_Status statut;
  /* Ouverture du fichier "Reference01_img1.bin" en lecture */
  code = MPI_File_open(comm2d, "Reference01_img1.bin", MPI_MODE_RDONLY,MPI_INFO_NULL, &descripteur);
  /* Test pour savoir si ouverture du fichier est correcte */
  if (code != MPI_SUCCESS) {
    fprintf(stderr, "ATTENTION erreur lors ouverture du fichier");
    MPI_Abort(comm2d, 2);
  }

  MPI_Datatype mysubarray;

  
  double * R_local=malloc((tab_bounds[1]-tab_bounds[0]+3)*(tab_bounds[3]-tab_bounds[2]+3)*sizeof(double));
  double ** R_local_mat=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
 
  if(R_local==NULL)
    printf("Erreur dans l'allocation mÃ©moire de R_local-- \n"); 
  
  int starts[2] = {tab_bounds[0]-1,tab_bounds[2]-1};
  int subsizes[2]  = {tab_bounds[1]-tab_bounds[0]+3,tab_bounds[3]-tab_bounds[2]+3};
  int bigsizes[2]  = {M_R, N_R};
  
  /*printf("Je suis le processus %d\n"
  "borne infÃ©rieure des lignes : %d\n" 
  "borne infÃ©rieure des colonnes : %d\n" 
  "borne supÃ©rieure des lignes : %d\n"
  "borne supÃ©rieure des colonnes : %d\n"
   ,rang,tab_bounds[0]-2,tab_bounds[2]-2,tab_bounds[0]-2+tab_bounds[1]-tab_bounds[0]+5-1,tab_bounds[2]-2+tab_bounds[3]-tab_bounds[2]+5-1);*/
  /*if(rang==3){ 
  printf("Je suis le processus %d\n"
  "taille de la sous-matrice %d x %d",
  rang,subsizes[0],subsizes[1]);
  }*/

  MPI_Type_create_subarray(2,bigsizes, subsizes, starts,
                                 MPI_ORDER_C, MPI_DOUBLE, &mysubarray);
  MPI_Type_commit(&mysubarray);
  
 
  deplacement_initial=0;
 
  MPI_File_set_view(descripteur,deplacement_initial,MPI_DOUBLE,mysubarray,"native",MPI_INFO_NULL);
  MPI_File_read(descripteur,R_local,(tab_bounds[1]-tab_bounds[0]+3)*(tab_bounds[3]-tab_bounds[2]+3),MPI_DOUBLE,&statut);

  MPI_File_close(&descripteur);
  

  for(int i=0;i<(tab_bounds[1]-tab_bounds[0]+3);i++){
     
     R_local_mat[i]=&(R_local[i*(tab_bounds[3]-tab_bounds[2]+3)]);
     
  }
 
   int ix,iy;

   double ** g_global_not_resized=allocarray(M_R,N_R);
   double * g_global_not_resized_vect=malloc(M_R*N_R*sizeof(double));

   double ** g_global_extended=allocarray(M_R+2*(iw+im2-1),N_R+2*(iw+im2-1));
   double * g_global_extended_vect=malloc((M_R+2*(iw+im2-1))*(N_R+2*(iw+im2-1))*sizeof(double));
   
   FILE * data_F0 = fopen("edge_detector.bin","r");
   fread(g_global_not_resized_vect,sizeof(g_global_not_resized_vect),M_R*N_R,data_F0);
   fclose(data_F0);
   for (int i=0;i<M_R;i++) {
	        g_global_not_resized[i] = &(g_global_not_resized_vect[i*N_R]);
   }
   
   for(int i=0;i<M_R+2*(iw+im2-1);i++){

      if(i<iw+im2-1) ix=iw+im2-1-i;
      else if(i>M_R+iw+im2-2) ix=2*M_R+iw+im2-1-i-2;
      else ix=i-(iw+im2-1);

      for(int j=0;j<N_R+2*(iw+im2-1);j++){
      
          if(j<iw+im2-1) iy=iw+im2-1-j;  
          else  if (j>N_R+iw+im2-2) iy=2*N_R+iw+im2-1-j-2;
          else iy=j-(iw+im2-1);

          g_global_extended[i][j]=g_global_not_resized[ix][iy];

      }
   }
   
   double ** g_local_extended=allocarray(tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1,tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1);
  

   for(int i=0;i<tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1;i++){
       for(int j=0;j<tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1;j++){
       g_local_extended[i][j]=g_global_extended[tab_bounds[0]-1+i][tab_bounds[2]-1+j]; 
       }
   }





   double ** R_global_not_resized=allocarray(M_R,N_R);
   double * R_global_not_resized_vect=malloc(M_R*N_R*sizeof(double));

   double ** R_global_extended=allocarray(M_R+2*(iw+im2-1),N_R+2*(iw+im2-1));
   double * R_global_extended_vect=malloc((M_R+2*(iw+im2-1))*(N_R+2*(iw+im2-1))*sizeof(double));
   
   
   FILE * data_F = fopen("Reference01_img1.bin","r");
   fread(R_global_not_resized_vect,sizeof(R_global_not_resized_vect),M_R*N_R,data_F);
   fclose(data_F);
   for (int i=0;i<M_R;i++) {
	        R_global_not_resized[i] = &(R_global_not_resized_vect[i*N_R]);
   }
   
   for(int i=0;i<M_R+2*(iw+im2-1);i++){

      if(i<iw+im2-1) ix=iw+im2-1-i;
      else if(i>M_R+iw+im2-2) ix=2*M_R+iw+im2-1-i-2;
      else ix=i-(iw+im2-1);

      for(int j=0;j<N_R+2*(iw+im2-1);j++){
      
          if(j<iw+im2-1) iy=iw+im2-1-j;  
          else  if (j>N_R+iw+im2-2) iy=2*N_R+iw+im2-1-j-2;
          else iy=j-(iw+im2-1);

          R_global_extended[i][j]=R_global_not_resized[ix][iy];

      }
   }
   
   double ** R_local_extended=allocarray(tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1,tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1);

   for(int i=0;i<tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1;i++){
       for(int j=0;j<tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1;j++){
       R_local_extended[i][j]=R_global_extended[tab_bounds[0]-1+i][tab_bounds[2]-1+j]; 
       }
   }
  

   
   
   double ** Ttilde_global_not_resized=allocarray(M_R,N_R);
   double * Ttilde_global_not_resized_vect=malloc(M_R*N_R*sizeof(double));

   double ** Ttilde_global_extended=allocarray(M_R+2*(iw+im2-1),N_R+2*(iw+im2-1));
   double * Ttilde_global_extended_vect=malloc((M_R+2*(iw+im2-1))*(N_R+2*(iw+im2-1))*sizeof(double));
   
   FILE * data_F2 = fopen("Template01_img2.bin","r");
   fread(Ttilde_global_not_resized_vect,sizeof(Ttilde_global_not_resized_vect),M_R*N_R,data_F2);
   fclose(data_F2);
   for (int i=0;i<M_R;i++) {
	        Ttilde_global_not_resized[i] = &(Ttilde_global_not_resized_vect[i*N_R]);
   }
   
    /*if(rang==0){
    for(int i=0;i<M_R;i++){
			for(int j=0;j<N_R;j++){

                        
                        printf("%lf\n",Ttilde_global_not_resized[i][j]); 
                        }
    }
    }

   MPI_Abort(comm2d,59);*/

   for(int i=0;i<M_R+2*(iw+im2-1);i++){

      if(i<iw+im2-1) ix=iw+im2-1-i;
      else if(i>M_R+iw+im2-2) ix=2*M_R+iw+im2-1-i-2;
      else ix=i-(iw+im2-1);

      for(int j=0;j<N_R+2*(iw+im2-1);j++){
      
          if(j<iw+im2-1) iy=iw+im2-1-j;  
          else  if (j>N_R+iw+im2-2) iy=2*N_R+iw+im2-1-j-2;
          else iy=j-(iw+im2-1);

          Ttilde_global_extended[i][j]=Ttilde_global_not_resized[ix][iy];

      }
   }
   
   
  
   double ** Ttilde_local_extended=allocarray(tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1,tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1);
   double ** Ttilde_local_extended_new=allocarray(tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1,tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1);

   for(int i=0;i<tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1;i++){
       for(int j=0;j<tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1;j++){
       Ttilde_local_extended[i][j]=Ttilde_global_extended[tab_bounds[0]-1+i][tab_bounds[2]-1+j]; 
       }
   }
   /* if(rang==1){
    FILE * final_Ttilde;
                final_Ttilde=fopen("checking_Ttilde_extended.txt","w"); 

		for(int i=0;i<tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1;i++){
			for(int j=0;j<tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1;j++){
				fprintf(final_Ttilde,"%lf\t",Ttilde_local_extended[i][j]);
			}
			fprintf(final_Ttilde,"\n");
		}
		fclose(final_Ttilde);

   }
   MPI_Barrier(comm2d);
   MPI_Abort(comm2d,59);*/
    copie_mat2D(Ttilde_local_extended_new, Ttilde_local_extended,(tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1),(tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1));
    communication_Ttilde(Ttilde_local_extended,tab_bounds,comm2d,voisin,voisin_diagonale,iw,iw2,im,im2);
  
    /*if(rang==1){
    FILE * final_Ttilde;
                final_Ttilde=fopen("checking_Ttilde_extended.txt","w"); 

		for(int i=0;i<tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1;i++){
			for(int j=0;j<tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1;j++){
				fprintf(final_Ttilde,"%lf\t",Ttilde_local_extended[i][j]);
			}
			fprintf(final_Ttilde,"\n");
		}
		fclose(final_Ttilde);

   }
   MPI_Barrier(comm2d);
   MPI_Abort(comm2d,59);*/	
   /************************VERIFIER CETTE PARTIE*****************/

    if (iNbNeigh>iw*iw-4)
    {
        iNbNeigh = iw*iw;
        IncludeCloseNeigh = NO;
    }
    else
    {
        
        if (IncludeCloseNeigh==YES ) iNbNeigh += 4;
    }

    /*****VERIFIER LES ALLOCATIONS*****/

    double ***pfWmat_local=NULL;
    int ***piYmat_local=NULL;
    int **piSizeNeighmat_local=NULL;
    malloc3ddouble(&pfWmat_local,(tab_bounds[1]-tab_bounds[0]+1+2*iw2), (tab_bounds[3]-tab_bounds[2]+1+2*iw2),iNbNeigh);
    malloc3dint(&piYmat_local,(tab_bounds[1]-tab_bounds[0]+1+2*iw2), (tab_bounds[3]-tab_bounds[2]+1+2*iw2),2*iNbNeigh);
    malloc2dint(&piSizeNeighmat_local,(tab_bounds[1]-tab_bounds[0]+1+2*iw2),(tab_bounds[3]-tab_bounds[2]+1+2*iw2));
    

   calcul_poids(M_R,N_R,im,iw,h,NbNeigh,IncludeCloseNeigh,tab_bounds,R_local_extended,pfWmat_local,piYmat_local,piSizeNeighmat_local);
  
  /*if(rang==0){
     for(int i=0;i<(tab_bounds[1]-tab_bounds[0]+1+2*iw2);i++){

         for(int j=0;j<(tab_bounds[3]-tab_bounds[2]+1+2*iw2);j++){

             for(int k=0;k<iNbNeigh;k++){

            //if(piYmat_local[i][j][0]>=tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1||piYmat_local[i][j][0]<0||piYmat_local[i][j][1]<0||piYmat_local[i][j][1]>=tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1) printf("erreur");
            //if(piYmat_local[i][j][2*k]>=tab_bounds[3]-tab_bounds[2]+2*(iw+im2)+1||piYmat_local[i][j][2*k]<0||piYmat_local[i][j][2*k+1]<0||piYmat_local[i][j][2*k+1]>=tab_bounds[1]-tab_bounds[0]+2*(iw+im2)+1) printf("erreur");
            
             if(pfWmat_local[i][j][k]>1) printf("%f\n",pfWmat_local[i][j][k]);
            }
         }
         //printf("\n");
     }

  /* }*/   

 
  



   double ** U1_local_mat=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   double ** U1_local_mat_new=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));/*--> direction des colonnes, des x dans le rÃ©fÃ©rentiel dÃ©formations*/
   double ** U2_local_mat=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   double ** U2_local_mat_new=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));/*--> direction des lignes, des y dans le rÃ©fÃ©rentiel dÃ©formations*/
   init_mat( U1_local_mat,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   init_mat( U1_local_mat_new,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   init_mat( U2_local_mat,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   init_mat( U2_local_mat_new,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));

   /******Variables de splitting******/

   double ** V11_local_mat=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   double ** V12_local_mat=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   double ** V21_local_mat=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   double ** V22_local_mat=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   init_mat(V11_local_mat,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   init_mat(V12_local_mat,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   init_mat(V21_local_mat,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   init_mat(V22_local_mat,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));

   double ** V11_local_mat_new=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   double ** V12_local_mat_new=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   double ** V21_local_mat_new=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   double ** V22_local_mat_new=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   copie_mat2D(V11_local_mat_new,V11_local_mat,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   copie_mat2D(V12_local_mat_new,V12_local_mat,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   copie_mat2D(V21_local_mat_new,V21_local_mat,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   copie_mat2D(V22_local_mat_new,V22_local_mat,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
   /* Mesure du temps en seconde dans la boucle en temps */

   t1 = MPI_Wtime();
   
   resolution(R_local_mat,g_local_extended,M_R,N_R,U1_local_mat,U2_local_mat,V11_local_mat,V12_local_mat,V21_local_mat,V22_local_mat,U1_local_mat_new,U2_local_mat_new, V11_local_mat_new,V12_local_mat_new,V21_local_mat_new,V22_local_mat_new,Ttilde_local_extended,Ttilde_local_extended_new,tab_bounds,voisin,voisin_diagonale,rang,theta_lip, ntx,nty, comm2d,im,iw,pfWmat_local,piYmat_local,piSizeNeighmat_local);

  /* Mesure du temps a la sortie de la boucle */
   
   t2 = MPI_Wtime();

   if (rang == 0) {
    /* Affichage du temps de convergence par le processus 0 */
    printf("Convergence en %f secs\n", t2-t1);
   }
    
  

  /****LibÃ©ration mÃ©moire****/
  free(R_local),free(R_local_mat),free(g_local_extended); 
  free(U1_local_mat), free(U1_local_mat_new),free(U2_local_mat),free(U2_local_mat_new);
  free(V11_local_mat),free(V12_local_mat),free(V21_local_mat),free(V22_local_mat);
  free(V11_local_mat_new),free(V12_local_mat_new),free(V21_local_mat_new),free(V22_local_mat_new);
  finalisation_mpi();
  
  return 0;

}
