#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include "functions.h"
#include "dimens.h"

#define SWAP(a,b,tmp) tmp=a; a=b; b=tmp
#define YES 1
#define NO 0


void calcul_poids(int iNy, int iNx, int im, int iw, double fh, int iNbNeigh, int iIncludeCloseNeigh,int * tab_bounds,double ** R_local_extended,double *** pfW,int *** piY,int ** piSizeNeigh)
{
    int i,j,k,i2,i3,iTmp,j2,j3; 
    int ic1,ixp1,ixp2,iyp1,iyp2;
    int iNbBestNeigh,iNbNeigh2,iNbNeigh4,iNbNeighToSort,iIsolatedPt,iStartCpt;
    int idy,idx,ixe,iye,ixt,iyt,ix,iy;
    int istart, iend, imiddle, icurrent; 
    double fDif,fDist,fcurrent,fTmp;
    int iXd;
    double * pfW2;
    double * pfW2b;
    
    int * pidx;
    int * pidy;
    int iw2=(iw-1)/2;
    int im2=(im-1)/2;
    int isqw2 = iw*iw;

    double **** pfWe;
    pfWe = (double ****) calloc( (unsigned)(tab_bounds[1]-tab_bounds[0]+1+2*iw2), sizeof(double***) );
    for(i=0;i<tab_bounds[1]-tab_bounds[0]+1+2*iw2;i++){
	pfWe[i]=(double***)calloc(tab_bounds[3]-tab_bounds[2]+1+2*iw2,sizeof(double **));
	for(j=0;j<tab_bounds[3]-tab_bounds[2]+1+2*iw2;j++){
		pfWe[i][j]=(double **)calloc(iw,sizeof(double*));
		for(k=0;k<iw;k++){
			pfWe[i][j][k]=(double *)calloc(iw,sizeof(double));
		}
	}
    }

  
   

    if (!pfWe)
        printf("Memory allocation failure\n");

    pfW2 = (double *) calloc( (unsigned)(iw*iw), sizeof(double) );
    if (!pfW2)
        printf("Memory allocation failure\n");

    pidx = (int *) calloc( (unsigned)(iw*iw), sizeof(int) );
    if (!pidx)
        printf("Memory allocation failure\n");
    
    pidy = (int *) calloc( (unsigned)(iw*iw), sizeof(int) );
    if (!pidy)
        printf("Memory allocation failure\n");


  
    time_t  start_time, end_time;
    time_t  start_time2, end_time2;
    
    start_time = clock();
        
    printf("\nStart NL-Weights\n");
    
    printf("iNy= %i, iNx= %i, im= %i, iw= %i, fh= %.3f\n",iNy,iNx,im,iw,fh);
        
    
    ic1 = im2+iw2;
    printf("iNbNeigh= %i, im2= %i, iw2= %i, ic1= %i\n",iNbNeigh,im2,iw2,ic1);
        
    if (iNbNeigh>iw*iw-4)
    {
        iNbNeigh = iw*iw;
        iNbBestNeigh = iNbNeigh;
        iIncludeCloseNeigh = NO;
    }
    else
    {
        iNbBestNeigh = iNbNeigh;
        if ( iIncludeCloseNeigh==YES ) iNbNeigh += 4;
    }
    printf("iNbNeigh= %i, iNbBestNeigh= %i, iIncludeCloseNeigh= %i\n",iNbNeigh,iNbBestNeigh,iIncludeCloseNeigh);
    
    iNbNeigh2 = 2*iNbNeigh;
    iNbNeigh4 = 4*iNbNeigh;
   
    double ** pfSdx=allocarray(tab_bounds[1]-tab_bounds[0]+iw+2*im2,tab_bounds[3]-tab_bounds[2]+iw+2*im2);
   
   

    pfW2b = (double *) calloc( (unsigned)(iNbNeigh), sizeof(double) );
    if (!pfW2b)
        printf("Memory allocation failure\n");
    int * pidxb;
    int * pidyb;
    pidxb = (int *) calloc( (unsigned)(iNbNeigh), sizeof(int) );
    if (!pidxb)
        printf("Memory allocation failure\n");
    
    pidyb = (int *) calloc( (unsigned)(iNbNeigh), sizeof(int) );
    if (!pidyb)
        printf("Memory allocation failure\n");
 

    printf("Step #1 (init): Time= %.3f sec",difftime(end_time2,start_time2)/1000);

      /* compute differences between patches*/
    start_time2 = clock();
    for (idy=-iw2;idy<=iw2;idy++)
        for (idx=-iw2;idx<=iw2;idx++)
    {
        
        /* compute translate image S_{dx,dy}(ixe,iye)*/
        /* clear image*/

        init_mat(pfSdx,(tab_bounds[1]-tab_bounds[0]+iw+2*im2),(tab_bounds[3]-tab_bounds[2]+iw+2*im2));
        
        ixe=iw2+1; iye=iw2+1;
        ixt=ixe+idx;
        iyt=iye+idy;
 
        fDif = R_local_extended[iye][ixe]-R_local_extended[iyt][ixt];
        pfSdx[iye-(iw2+1)][ixe-(iw2+1)] = fDif*fDif;
        
        ixe=iw2+1;
#pragma omp simd        
        for (iye=iw2+2;iye<iw+im2+tab_bounds[1]-tab_bounds[0]+1+iw2+im2;iye++)
        {
            ixt=ixe+idx;
            iyt=iye+idy;
            
            fDif = R_local_extended[iye][ixe]-R_local_extended[iyt][ixt];
            pfSdx[iye-(iw2+1)][ixe-(iw2+1)] =pfSdx[iye-1-(iw2+1)][ixe-(iw2+1)]+fDif*fDif;
         

        }
       
        iye=iw2+1;
#pragma omp simd        
        for (ixe=iw2+2;ixe<iw+im2+tab_bounds[3]-tab_bounds[2]+1+iw2+im2;ixe++)
        {
            ixt=ixe+idx;
            iyt=iye+idy;
            
            fDif = R_local_extended[iye][ixe]-R_local_extended[iyt][ixt];
            pfSdx[iye-(iw2+1)][ixe-(iw2+1)] =pfSdx[iye-(iw2+1)][ixe-1-(iw2+1)] + fDif*fDif;
        }
         
        for (ixe=iw2+2;ixe<iw+im2+tab_bounds[3]-tab_bounds[2]+1+iw2+im2;ixe++)
#pragma omp simd        
            for (iye=iw2+2;iye<iw+im2+tab_bounds[1]-tab_bounds[0]+1+iw2+im2;iye++)
        {
            ixt=ixe+idx;
            iyt=iye+idy;
            
            fDif = R_local_extended[iye][ixe]-R_local_extended[iyt][ixt];
            pfSdx[iye-(iw2+1)][ixe-(iw2+1)] = pfSdx[iye-(iw2+1)][ixe-1-(iw2+1)] + pfSdx[iye-1-(iw2+1)][ixe-(iw2+1)]- pfSdx[iye-1-(iw2+1)][ixe-1-(iw2+1)]+ fDif*fDif;
        }
        
        /* Original Image zone : */
#pragma omp simd        
      for (iye=iw+im2-iw2;iye<iw+im2+tab_bounds[1]-tab_bounds[0]+1+iw2;iye++)
            for (ixe=iw+im2-iw2;ixe<iw+im2+tab_bounds[3]-tab_bounds[2]+1+iw2;ixe++)       
        {
            
            /* Sdx(iy-s,ix-s)-Sdx(iy+s,ix+s) to get the patch difference*/
            iyp1=iye-im2;
            ixp1=ixe-im2;
            iyp2=iye+im2;
            ixp2=ixe+im2;
           
            /* dist*/
            fDist = pfSdx[iyp2-(iw2+1)][ixp2-(iw2+1)] - pfSdx[iyp2-(iw2+1)][ixp1-(iw2+1)] - pfSdx[iyp1-(iw2+1)][ixp2-(iw2+1)] + pfSdx[iyp1-(iw2+1)][ixp1-(iw2+1)];
            pfWe[iye-(iw+im2-iw2)][ixe-(iw+im2-iw2)][idy+iw2][idx+iw2]=fDist;
            

           
    
        
        }
    }
    end_time2 = clock();
    printf("\nStep #2 (distance between patches): Time= %.3f sec\n",difftime(end_time2,start_time2)/1000);
   
    /*Tout est cohérent jusqu'ici*/
    /* C'est après qu'il manque des calculs*/

    /* CENTER*/
    start_time2 = clock();
    for (iy=iw+im2-iw2; iy<iw+im2+tab_bounds[1]-tab_bounds[0]+1+iw2; iy++)
        for(ix=iw+im2-iw2; ix<iw+im2+tab_bounds[3]-tab_bounds[2]+1+iw2; ix++)
    {
       

        for (idy=-iw2; idy<= iw2; idy++)
            for(idx=-iw2; idx<= iw2; idx++)
        {
                iXd=(idx+iw2)*iw+ (idy+iw2);
                pfW2[iXd] = pfWe[iy-(iw+im2-iw2)][ix-(iw+im2-iw2)][idy+iw2][idx+iw2];// stockage ligne après ligne
                pidx[iXd] = idx; 
                pidy[iXd] = idy;
            }
        
        /* 4 neighbors*/
        if ( iIncludeCloseNeigh==YES )
        {
            idx = 1; idy = 0; pfW2[(idx+iw2)*iw+ (idy+iw2)] = 0.0;
            idx = -1; idy = 0; pfW2[(idx+iw2)*iw+ (idy+iw2)] = 0.0;
            idx = 0; idy = 1; pfW2[(idx+iw2)*iw+ (idy+iw2)] = 0.0;
            idx = 0; idy = -1; pfW2[(idx+iw2)*iw+ (idy+iw2)] = 0.0;
            iNbNeighToSort = iNbNeigh;
        }
        else        
            iNbNeighToSort = iNbBestNeigh;
        
        
        for (i2=0; i2< iNbNeighToSort; i2++)  pfW2b[i2] = 1024.0;
        for (i2=0; i2< isqw2; i2++)
        {
            fcurrent = pfW2[i2];
            if ( fcurrent<pfW2b[iNbNeighToSort-1] )
            {
                /* dichotomy*/
                istart = 0;
                iend = iNbNeighToSort-1;
                while ( iend-istart>1 )
                {
                    imiddle = (iend-istart)/2;
                    if (pfW2b[istart+imiddle] > fcurrent)
                        iend = istart+imiddle;
                    else
                        istart = istart+imiddle;
                }
                if (pfW2b[istart] > fcurrent)
                    icurrent = istart;
                else
                    icurrent = iend;
                
                /* shifting*/
#pragma omp simd        
                for(i3=iNbNeighToSort-2; i3>=icurrent; i3--)
                {
                    SWAP(pfW2b[i3],pfW2b[i3+1],fTmp);
                    SWAP(pidxb[i3],pidxb[i3+1],iTmp);
                    SWAP(pidyb[i3],pidyb[i3+1],iTmp);
                }
                
                /* new value*/
                pfW2b[icurrent] = fcurrent;
                pidxb[icurrent] = pidx[i2];
                pidyb[icurrent] = pidy[i2];
                
            }
        } /* end for (i2=0; i2< isqw2; i2++)*/
        
        
        
        /* 4 neighbors*/
        if ( iIncludeCloseNeigh==YES )
        {
            icurrent = 0;
            idx = 0; idy = 0; pfW2b[icurrent] = pfW2b[1+4]; /* give same weight to x,y and Yx,Yy*/
            pidxb[icurrent] = idx; pidyb[icurrent] = idy; icurrent++;
            idx = 1; idy = 0; pfW2b[icurrent] = pfWe[iy-(iw+im2-iw2)][ix-(iw+im2-iw2)][idy+iw2][idx+iw2];
            pidxb[icurrent] = idx; pidyb[icurrent] = idy; icurrent++;
            idx = -1; idy = 0; pfW2b[icurrent] = pfWe[iy-(iw+im2-iw2)][ix-(iw+im2-iw2)][idy+iw2][idx+iw2];
            pidxb[icurrent] = idx; pidyb[icurrent] = idy; icurrent++;
            idx = 0; idy = 1; pfW2b[icurrent] = pfWe[iy-(iw+im2-iw2)][ix-(iw+im2-iw2)][idy+iw2][idx+iw2];
            pidxb[icurrent] = idx; pidyb[icurrent] = idy; icurrent++;
            idx = 0; idy = -1; pfW2b[icurrent] = pfWe[iy-(iw+im2-iw2)][ix-(iw+im2-iw2)][idy+iw2][idx+iw2];
            pidxb[icurrent] = idx; pidyb[icurrent] = idy;
        }
        else
            pfW2b[0] = pfW2b[1]; /* give same weight to x,y and Yx,Yy*/
        
        
        /* compute symmetric W*/

        //iy=iw+im2-iw2; iy<iw+im2+tab_bounds[1]-tab_bounds[0]+1+iw2
        //

        for (i=0; i< iNbNeigh; i++)
        {
            idx = pidxb[i]; 
            idy = pidyb[i];
     
           if (exp(-pfW2b[i]/fh)>0.2) pfW[iy-(iw+im2-iw2)][ix-(iw+im2-iw2)][i] = exp(-pfW2b[i]/fh); else pfW[iy-(iw+im2-iw2)][ix-(iw+im2-iw2)][i] = 0.2;
                
                 piY[iy-(iw+im2-iw2)][ix-(iw+im2-iw2)][2*i] = ix+idx;
                 piY[iy-(iw+im2-iw2)][ix-(iw+im2-iw2)][2*i+1] = iy+idy;
                 piSizeNeigh[iy-(iw+im2-iw2)][ix-(iw+im2-iw2)]=iNbNeigh;
           
		
        } 
    
        
   
        
        } /* END*/
    end_time2 = clock();
    printf("Step #3 (sort m best values): Time= %.3f sec\n",difftime(end_time2,start_time2)/1000);    
    


}
