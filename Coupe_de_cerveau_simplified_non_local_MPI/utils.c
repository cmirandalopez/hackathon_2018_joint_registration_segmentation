#include <stdlib.h>
#include <stdio.h>

   double **allocarray(int Nlig,int Mcol) {
    double **array2 = malloc( Nlig* sizeof( double * ) );
    int i;

    if( array2 != NULL ){
        array2[0] = malloc(Nlig * Mcol * sizeof( double ) );
        if( array2[ 0 ] != NULL ) {
            for( i = 1; i < Nlig; i++ )
                array2[i] = array2[0] + i * Mcol;
        }

        else {
            free(array2);
            array2 = NULL;
            printf("Erreur dans l'allocation mÃ©moire -- phase 2\n"); 
        }
    }
    return array2;
    }

   int **allocarray_int(int Nlig,int Mcol) {
    int **array2 = malloc( Nlig* sizeof( int * ) );
    int i;

    if( array2 != NULL ){
        array2[0] = malloc(Nlig * Mcol * sizeof( int ) );
        if( array2[ 0 ] != NULL ) {
            for( i = 1; i < Nlig; i++ )
                array2[i] = array2[0] + i * Mcol;
        }

        else {
            free(array2);
            array2 = NULL;
            printf("Erreur dans l'allocation mÃ©moire -- phase 2\n"); 
        }
    }
    return array2;
    }


   int malloc3ddouble(double ****array, int n, int m, int k) {

    /* allocate the n*m contiguous items */
    double *p = (double *)malloc(n*m*k*sizeof(double));
    if (!p) return -1;

    /* allocate the row pointers into the memory */
    (*array) = (double ***)malloc(n*sizeof(double**));
    if (!(*array)) {
       free(p);
       return -1;
    }

    /* set up the pointers into the contiguous memory */
    for (int i=0; i<n; i++){ 
       (*array)[i] = (double **)malloc(sizeof(double *)*m);
    	for(int j=0;j<m;j++)
	(*array)[i][j]=&(p[i*m*k+j*k]);
    }
    return 0;
    }

   void printarr(double **data, int nlig,int mcol, char *str) {    
    printf("-- %s --\n", str);
    for (int i=0; i<nlig; i++) {
        for (int j=0; j<mcol; j++) {
            printf("%3f ", data[i][j]);
        }
        printf("\n");
    }
   }

   int malloc3dint(int ****array, int n, int m, int k) {

    /* allocate the n*m contiguous items */
    int *p = (int *)malloc(n*m*k*sizeof(int));
    if (!p) return -1;

    /* allocate the row pointers into the memory */
    (*array) = (int ***)malloc(n*sizeof(int**));
    if (!(*array)) {
       free(p);
       return -1;
    }

    /* set up the pointers into the contiguous memory */
    for (int i=0; i<n; i++){ 
       (*array)[i] = (int **)malloc(sizeof(int *)*m);
    	for(int j=0;j<m;j++)
	(*array)[i][j]=&(p[i*m*k+j*k]);
    }

   
    
    return 0;
   }
 
    int malloc2dint(int ***array, int n, int m) {

    /* allocate the n*m contiguous items */
    int *p = (int *)malloc(n*m*sizeof(int));
    if (!p) return -1;

    /* allocate the row pointers into the memory */
    (*array) = (int **)malloc(n*sizeof(int*));
    if (!(*array)) {
       free(p);
       return -1;
    }

    /* set up the pointers into the contiguous memory */
    for (int i=0; i<n; i++) 
       (*array)[i] = &(p[i*m]);

    return 0;
}

void init_mat(double ** tab,int Mlig,int Ncol){

	int i,j;
	for(i=0;i<Mlig;i++){
		for(j=0;j<Ncol; j++){
			tab[i][j]=0.0;
		}
	}
}


void init_mat_ones(double ** tab,int Mlig,int Ncol){

	int i,j;
	for(i=0;i<Mlig;i++){
		for(j=0;j<Ncol; j++){
			tab[i][j]=1.0;
		}
	}
}


void addition(int M_R, int N_R,int ** Id,double ** U,double ** IdU){
	int i,j;



	for(i=0;i<M_R;i++){
		for(j=0;j<N_R;j++){
			IdU[i][j]=1.0*Id[i][j]+U[i][j];

		}
	}


}

void copie_mat2D(double** dest, double ** src, int M,int n){

	int i,j;
	for(i=0;i<M;i++){
		for(j=0;j<n;j++){
			dest[i][j]=src[i][j];
		}
	}
}
