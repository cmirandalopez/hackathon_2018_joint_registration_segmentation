#include <stdlib.h>
#include <math.h>

#include "dimens.h"

inline double func_b(double x){
	double res;

	if(x>=-2 && x<-1){
		res=(x+2.0)*(x+2.0)*(x+2.0);
	}
	else if(x>=-1 && x<0){
		res=-x*x*x-2.0*(x+1.0)*(x+1.0)*(x+1.0)+6.0*(x+1.0);
	}
	else if(0<=x && x<1){

		res=x*x*x+2.0*(x-1.0)*(x-1.0)*(x-1.0)-6.0*(x-1.0);
	}
	else if(x>=1 && x<2){

		res=(2.0-x)*(2.0-x)*(2.0-x);
	}
	else res=0;

	return(res);
}

inline double diff_b(double x){
	double res;

	if(x>=-2 && x<-1){
		res=3.0*(x+2.0)*(x+2.0);
	}
	else if(x>=-1 && x<0){
		res=-3.0*x*x-6.0*(x+1.0)*(x+1.0)+6.0;
	}
	else if(0<=x && x<1){

		res=3.0*x*x+6.0*(x-1.0)*(x-1.0)-6.0;
	}
	else if(x>=1 && x<2){

		res=-3.0*(2.0-x)*(2.0-x);
	}
	else res=0;

	return(res);
}

//interpolation 2D
void interpol2D(int M_Re,int N_Re,double ** Id2U2_local,double ** Id1U1_local,double ** C,double ** interpolant_local,double ** dx_interpolant_local,double ** dy_interpolant_local,int nlig,int ncol){

	
	int p,q,i,j;
	double xi_1,xi_2;

	for(i=0;i<M_Re;i++){
#pragma omp simd        

		for(j=0;j<N_Re;j++){

                       
			p=floor(Id1U1_local[i][j]);
			q=floor(Id2U2_local[i][j]);
                        

                        if(q<2){q=2;}//printf("q:%d, i=%d ,j=%d U2=%lf \n", q,i,j, comp_phi2[i][j]-i-1);
			if(p<2){p=2;}//printf("p:%d  i=%d ,j=%d U1=%lf\n", p,i,j, comp_phi1[i][j]-j-1);
			if(q>nlig-2){q=nlig-2;}//printf("q:%d  i=%d ,j=%d U2=%lf\n", q,i,j, comp_phi2[i][j]-i-1);  
			if(p>ncol-2){p=ncol-2;}//printf("p:%d  i=%d ,j=%d U1=%lf, %d\n", p,i,j, comp_phi1[i][j], bandwidth);
 
                        xi_1=Id1U1_local[i][j]-p;
			xi_2=Id2U2_local[i][j]-q;

                        /*if(rang==1){
                        printf("q-2: %d\t, p-2 : %d\n",q-2,p-2);
                        //printf("\n");
                        //printf("\n"); 
                        }*/

			interpolant_local[i][j]=C[q-2][p-2]*func_b(xi_1+1.0)*func_b(xi_2+1.0)+C[q-1][p-2]*func_b(xi_1+1.0)*func_b(xi_2)+
				C[q][p-2]*func_b(xi_1+1.0)*func_b(xi_2-1.0)+C[q+1][p-2]*func_b(xi_1+1.0)*func_b(xi_2-2.0)+
				C[q-2][p-1]*func_b(xi_1)*func_b(xi_2+1.0)+C[q-1][p-1]*func_b(xi_1)*func_b(xi_2)+C[q][p-1]*func_b(xi_1)*
				func_b(xi_2-1.0)+C[q+1][p-1]*func_b(xi_1)*func_b(xi_2-2.0)+C[q-2][p]*func_b(xi_1-1.0)*func_b(xi_2+1.0)+
				C[q-1][p]*func_b(xi_1-1.0)*func_b(xi_2)+C[q][p]*func_b(xi_1-1.0)*func_b(xi_2-1.0)+
				C[q+1][p]*func_b(xi_1-1.0)*func_b(xi_2-2.0)+C[q-2][p+1]*func_b(xi_1-2.0)*func_b(xi_2+1.0)+C[q-1][p+1]*
				func_b(xi_1-2.0)*func_b(xi_2)+C[q][p+1]*func_b(xi_1-2.0)*func_b(xi_2-1.0)+C[q+1][p+1]*func_b(xi_1-2.0)*
				func_b(xi_2-2.0);


			dx_interpolant_local[i][j]=C[q-2][p-2]*diff_b(xi_1+1.0)*func_b(xi_2+1.0)+C[q-1][p-2]*diff_b(xi_1+1.0)*func_b(xi_2)+
				C[q][p-2]*diff_b(xi_1+1.0)*func_b(xi_2-1.0)+C[q+1][p-2]*diff_b(xi_1+1.0)*func_b(xi_2-2.0)+
				C[q-2][p-1]*diff_b(xi_1)*func_b(xi_2+1.0)+C[q-1][p-1]*diff_b(xi_1)*func_b(xi_2)+C[q][p-1]*diff_b(xi_1)*
				func_b(xi_2-1.0)+C[q+1][p-1]*diff_b(xi_1)*func_b(xi_2-2.0)+C[q-2][p]*diff_b(xi_1-1.0)*func_b(xi_2+1.0)+
				C[q-1][p]*diff_b(xi_1-1.0)*func_b(xi_2)+C[q][p]*diff_b(xi_1-1.0)*func_b(xi_2-1.0)+
				C[q+1][p]*diff_b(xi_1-1.0)*func_b(xi_2-2.0)+C[q-2][p+1]*diff_b(xi_1-2.0)*func_b(xi_2+1.0)+C[q-1][p+1]*
				diff_b(xi_1-2.0)*func_b(xi_2)+C[q][p+1]*diff_b(xi_1-2.0)*func_b(xi_2-1.0)+C[q+1][p+1]*diff_b(xi_1-2.0)*
				func_b(xi_2-2.0);

			dy_interpolant_local[i][j]=C[q-2][p-2]*func_b(xi_1+1.0)*diff_b(xi_2+1.0)+C[q-1][p-2]*func_b(xi_1+1.0)*diff_b(xi_2)+
				C[q][p-2]*func_b(xi_1+1.0)*diff_b(xi_2-1.0)+C[q+1][p-2]*func_b(xi_1+1.0)*diff_b(xi_2-2.0)+
				C[q-2][p-1]*func_b(xi_1)*diff_b(xi_2+1.0)+C[q-1][p-1]*func_b(xi_1)*diff_b(xi_2)+C[q][p-1]*func_b(xi_1)*
				diff_b(xi_2-1.0)+C[q+1][p-1]*func_b(xi_1)*diff_b(xi_2-2.0)+C[q-2][p]*func_b(xi_1-1.0)*diff_b(xi_2+1.0)+
				C[q-1][p]*func_b(xi_1-1.0)*diff_b(xi_2)+C[q][p]*func_b(xi_1-1.0)*diff_b(xi_2-1.0)+
				C[q+1][p]*func_b(xi_1-1.0)*diff_b(xi_2-2.0)+C[q-2][p+1]*func_b(xi_1-2.0)*diff_b(xi_2+1.0)+C[q-1][p+1]*
				func_b(xi_1-2.0)*diff_b(xi_2)+C[q][p+1]*func_b(xi_1-2.0)*diff_b(xi_2-1.0)+C[q+1][p+1]*func_b(xi_1-2.0)*
				diff_b(xi_2-2.0);

		}
	}
}

