#include <mpi.h>

// Calcul
void resolution(double** R_local_mat,double ** g_local_extended,int M_R, int N_R, double** U1_local_mat, double** U2_local_mat,double* restrict * V11_local_mat,double* restrict * V12_local_mat,double* restrict * V21_local_mat,double* restrict * V22_local_mat,double** U1_local_mat_new, double** U2_local_mat_new,double* restrict * V11_local_mat_new,double* restrict * V12_local_mat_new,double* restrict * V21_local_mat_new,double* restrict * V22_local_mat_new,double * restrict * restrict Ttilde_local_extended,double * restrict * restrict Ttilde_local_extended_new, int * tab_bounds,int * voisin,int * voisin_diagonale,int rang,double theta_lip,int ntx,int nty,MPI_Comm comm2d,int im,int iw,double *** pfW,int *** piY,int ** piSizeNeigh);

 void calcul_V(int *, double **, double **, double **, double **, double **, double **, double **, double **, double **, double **, double **, double **, double, double, double, double, double, double, double, double);

 void calcul_U(int *, int *, int *, MPI_Comm, int, double, double, double **, double **, double **, double **, double **, double **, double **, double **, double **, double**, double **, double **, double **, double **, double, double, double);

 double heaviside(double, double);
 double dirac(double, double);
 double SQRT(double);
 void coeff_spline_multiscale(int, int, double **, double **, double);
 void calcul_poids(int, int, int, int, double, int, int, int *, double **, double ***, int ***, int **);

void interpol2D(int M_Re,int N_Re,double ** Id2U2_local,double ** Id1U1_local,double ** C,double ** interpolant_local,double ** dx_interpolant_local,double ** dy_interpolant_local,int nlig,int ncol);

// Communications MPI
 void cree_type(int *);
 void communication_V(double **, double **, double **, double **, int *, MPI_Comm, int *);
 void communication(double **, double **, int *, MPI_Comm, int *, int *);
 void communication_interpolation(double **, double **, double **, int *, MPI_Comm, int *);
 void communication_f(double **, int *, MPI_Comm, int *);
 void communication_Ttilde(double **, int *, MPI_Comm comm2d, int *, int *, int, int, int,int);
 void ecrire_mpi(double *, int, int, int *, MPI_Comm, char *);

// Fonctions utilitaires
 double **allocarray(int, int);
 int **allocarray_int(int, int);
 void init_mat(double **, int, int);
 void init_mat_ones(double **, int, int);
 void init_mat1D(double *, int);
 void copie_mat1D(double *, double *, int);
 void copie_mat2D(double **, double **, int, int);
 void swap_pointers(double **,double **);
 int malloc3ddouble(double ****, int, int, int);
 int malloc3dint(int ****, int, int, int);
 int malloc2dint(int ***, int, int);
 void printarr(double **, int, int, char *);


// Macro pour fonction utilitaire (verification de malloc)
#define communicator comm2d
#define checkalloc(ptr) \
    if (ptr == NULL) { \
          free(ptr); \
          printf("Erreur dans l'allocation mémoire\n");\
          MPI_Abort(communicator, -8); \
     }
