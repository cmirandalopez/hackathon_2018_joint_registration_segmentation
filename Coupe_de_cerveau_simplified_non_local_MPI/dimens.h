
#define Nelem_total_x (tab_bounds[1] - tab_bounds[0] + 3)
#define Nelem_total_y (tab_bounds[3] - tab_bounds[2] + 3)

#define Nelem (Nelem_total_x * Nelem_total_y)

//#define i][j ( (i) * Nelem_total_y + (j) )

// ---

#define interpolant_local(i, j) interpolant_local[i][j]
#define dx_interpolant_local(i, j) dx_interpolant_local[i][j]
#define dy_interpolant_local(i, j) dy_interpolant_local[i][j]

#define Id1U1_local(i, j) Id1U1_local[i][j]
#define Id2U2_local(i, j) Id2U2_local[i][j]

#define p1_local(i, j) p1_local[i][j]
#define p2_local(i, j) p2_local[i][j]
#define p1_local_new(i, j) p1_local_new[i][j]
#define p2_local_new(i, j) p2_local_new[i][j]

#define f_local(i, j) f_local[i][j]
#define R_local_mat(i, j) R_local_mat[i][j]
#define g_local_mat(i, j) g_local_mat[i][j]
#define T_tilde_local(i, j) T_tilde_local[i][j]

#define V11_local_mat(i, j) V11_local_mat[i][j]
#define V12_local_mat(i, j) V12_local_mat[i][j]
#define V21_local_mat(i, j) V21_local_mat[i][j]
#define V22_local_mat(i, j) V22_local_mat[i][j]

#define V11_local_mat_new(i, j) V11_local_mat_new[i][j]
#define V12_local_mat_new(i, j) V12_local_mat_new[i][j]
#define V21_local_mat_new(i, j) V21_local_mat_new[i][j]
#define V22_local_mat_new(i, j) V22_local_mat_new[i][j]

#define U1_local_mat(i, j) U1_local_mat[i][j]
#define U2_local_mat(i, j) U2_local_mat[i][j]
#define U1_local_mat_new(i, j) U1_local_mat_new[i][j]
#define U2_local_mat_new(i, j) U2_local_mat_new[i][j]
