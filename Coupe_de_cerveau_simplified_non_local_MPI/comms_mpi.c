#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "mpi_params.h"
#include "functions.h"
#include "dimens.h"

 void communication_V(double ** V11_local_mat, double ** V12_local_mat,double ** V21_local_mat, double ** V22_local_mat, int * tab_bounds,MPI_Comm comm2d,int * voisin) {

 
  /*****CrÃ©ation des types dÃ©rivÃ©s*****/
  MPI_Datatype type_ligne, type_colonne_mono;

  MPI_Type_contiguous(tab_bounds[3]-tab_bounds[2]+1,MPI_DOUBLE,&type_ligne);
  MPI_Type_commit(&type_ligne);
  
  MPI_Type_vector(tab_bounds[1]-tab_bounds[0]+1,1,tab_bounds[3]-tab_bounds[2]+3, MPI_DOUBLE, &type_colonne_mono);
  MPI_Type_commit(&type_colonne_mono); 
  /************************************/

  const int etiquette = 800;
  MPI_Status statut;
  int sx,ex,sy,ey;
  sx=1;
  sy=1;
  ex=tab_bounds[1]-tab_bounds[0]+1;
  ey=tab_bounds[3]-tab_bounds[2]+1;
 
  
  /* Envoi au voisin N et reception du voisin S */
  

  MPI_Sendrecv(&V11_local_mat[sx][sy], 1, type_ligne, voisin[North], etiquette, 
	       &V11_local_mat[ex+1][sy], 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  MPI_Sendrecv(&V12_local_mat[sx][sy], 1, type_ligne, voisin[North], etiquette, 
	       &V12_local_mat[ex+1][sy], 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut);

  MPI_Sendrecv(&V21_local_mat[sx][sy], 1, type_ligne, voisin[North], etiquette, 
	       &V21_local_mat[ex+1][sy], 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  MPI_Sendrecv(&V22_local_mat[sx][sy], 1, type_ligne, voisin[North], etiquette, 
	       &V22_local_mat[ex+1][sy], 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut);

  
  /* Envoi au voisin S et reception du voisin N */

  
  MPI_Sendrecv(&(V11_local_mat[ex][sy]), 1, type_ligne, voisin[S], etiquette, 
	       &(V11_local_mat[sx-1][sy]), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);

  MPI_Sendrecv(&(V12_local_mat[ex][sy]), 1, type_ligne, voisin[S], etiquette, 
	       &(V12_local_mat[sx-1][sy]), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
  MPI_Sendrecv(&(V21_local_mat[ex][sy]), 1, type_ligne, voisin[S], etiquette, 
	       &(V21_local_mat[sx-1][sy]), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);

  MPI_Sendrecv(&(V22_local_mat[ex][sy]), 1, type_ligne, voisin[S], etiquette, 
	       &(V22_local_mat[sx-1][sy]), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  

 
  /* Envoi au voisin W et  reception du voisin E */
 
  MPI_Sendrecv(&(V11_local_mat[sx][sy]), 1, type_colonne_mono , voisin[W], etiquette, 
		&(V11_local_mat[sx][ey+1]), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(V12_local_mat[sx][sy]), 1, type_colonne_mono , voisin[W], etiquette, 
		&(V12_local_mat[sx][ey+1]), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(V21_local_mat[sx][sy]), 1, type_colonne_mono , voisin[W], etiquette, 
		&(V21_local_mat[sx][ey+1]), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(V22_local_mat[sx][sy]), 1, type_colonne_mono , voisin[W], etiquette, 
		&(V22_local_mat[sx][ey+1]), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  

  /* Envoi au voisin E et  reception du voisin W */


  MPI_Sendrecv(&(V11_local_mat[sx][ey]), 1, type_colonne_mono, voisin[E], etiquette, 
		&(V11_local_mat[sx][sy-1]), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  MPI_Sendrecv(&(V12_local_mat[sx][ey]), 1, type_colonne_mono, voisin[E], etiquette, 
		&(V12_local_mat[sx][sy-1]), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  MPI_Sendrecv(&(V21_local_mat[sx][ey]), 1, type_colonne_mono, voisin[E], etiquette, 
		&(V21_local_mat[sx][sy-1]), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(V22_local_mat[sx][ey]), 1, type_colonne_mono, voisin[E], etiquette, 
		&(V22_local_mat[sx][sy-1]), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
 
  

}


   void communication(double **U1_local_mat,double **U2_local_mat, int * tab_bounds,MPI_Comm comm2d,int * voisin,int * voisin_diagonale) {

 
  /*****CrÃ©ation des types dÃ©rivÃ©s*****/
  MPI_Datatype type_ligne, type_colonne_mono;

  MPI_Type_contiguous(tab_bounds[3]-tab_bounds[2]+1,MPI_DOUBLE,&type_ligne);
  MPI_Type_commit(&type_ligne);
  
  MPI_Type_vector(tab_bounds[1]-tab_bounds[0]+1,1,tab_bounds[3]-tab_bounds[2]+3, MPI_DOUBLE, &type_colonne_mono);
  MPI_Type_commit(&type_colonne_mono); 
  /************************************/

  const int etiquette = 100;
  MPI_Status statut;
  int sx,ex,sy,ey;
  sx=1;
  sy=1;
  ex=tab_bounds[1]-tab_bounds[0]+1;
  ey=tab_bounds[3]-tab_bounds[2]+1;
 
  
  /* Envoi au voisin N et reception du voisin S */
  
  MPI_Sendrecv(&U1_local_mat[sx][sy], 1, type_ligne, voisin[North], etiquette, 
	       &U1_local_mat[ex+1][sy], 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  MPI_Sendrecv(&U2_local_mat[sx][sy], 1, type_ligne, voisin[North], etiquette, 
	       &U2_local_mat[ex+1][sy], 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut);

  
  
  /* Envoi au voisin S et reception du voisin N */
  MPI_Sendrecv(&(U1_local_mat[ex][sy]), 1, type_ligne, voisin[S], etiquette, 
	       &(U1_local_mat[sx-1][sy]), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
  MPI_Sendrecv(&(U2_local_mat[ex][sy]), 1, type_ligne, voisin[S], etiquette, 
	       &(U2_local_mat[sx-1][sy]), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
 
  

 
  /* Envoi au voisin W et  reception du voisin E */
 
  MPI_Sendrecv(&(U1_local_mat[sx][sy]), 1, type_colonne_mono , voisin[W], etiquette, 
		&(U1_local_mat[sx][ey+1]), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat[sx][sy]), 1, type_colonne_mono , voisin[W], etiquette, 
		&(U2_local_mat[sx][ey+1]), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
 

  /* Envoi au voisin E et  reception du voisin W */
 
  
  MPI_Sendrecv(&(U1_local_mat[sx][ey]), 1, type_colonne_mono, voisin[E], etiquette, 
		&(U1_local_mat[sx][sy-1]), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat[sx][ey]), 1, type_colonne_mono, voisin[E], etiquette, 
		&(U2_local_mat[sx][sy-1]), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  

  /*Envoi au voisin NE et reception du voisin SO*/

  
  MPI_Sendrecv(&(U1_local_mat[sx][ey]),1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		&(U1_local_mat[ex+1][sy-1]), 1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat[sx][ey]),1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		&(U2_local_mat[ex+1][sy-1]), 1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		comm2d, &statut);
  

  /*Envoi au voisin SO et reception du voisin NE*/


  MPI_Sendrecv(&(U1_local_mat[ex][sy]),1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		&(U1_local_mat[sx-1][ey+1]), 1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		comm2d, &statut);

  MPI_Sendrecv(&(U2_local_mat[ex][sy]),1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		&(U2_local_mat[sx-1][ey+1]), 1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		comm2d, &statut);


  /*Envoi au voisin ES et reception du voisin ON*/

  
  MPI_Sendrecv(&(U1_local_mat[ex][ey]),1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		&(U1_local_mat[sx-1][sy-1]), 1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat[ex][ey]),1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		&(U2_local_mat[sx-1][sy-1]), 1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		comm2d, &statut);
 
 

  /*Envoi au voisin ON et reception du voisin ES*/

  MPI_Sendrecv(&(U1_local_mat[sx][sy]),1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		&(U1_local_mat[ex+1][ey+1]), 1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat[sx][sy]),1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		&(U2_local_mat[ex+1][ey+1]), 1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		comm2d, &statut);
 


  /*Traitement des cas particuliers pour les envois diagonaux*/

  
  
  if(voisin[North]==MPI_PROC_NULL){  
  
  MPI_Sendrecv(&(U1_local_mat[sx-1][ey]),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(U1_local_mat[sx-1][sy-1]), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);

  
  MPI_Sendrecv(&(U2_local_mat[sx-1][ey]),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(U2_local_mat[sx-1][sy-1]), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);

 
  }

  if(voisin[North]==MPI_PROC_NULL){

 
  MPI_Sendrecv(&(U1_local_mat[sx-1][sy]),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(U1_local_mat[sx-1][ey+1]), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);
  
  MPI_Sendrecv(&(U2_local_mat[sx-1][sy]),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(U2_local_mat[sx-1][ey+1]), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);

  }


 

  if(voisin[W]==MPI_PROC_NULL){
   

  
  MPI_Sendrecv(&(U1_local_mat[ex][sy-1]),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(U1_local_mat[sx-1][sy-1]), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);
 MPI_Sendrecv(&(U2_local_mat[ex][sy-1]),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(U2_local_mat[sx-1][sy-1]), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);
  

  }

  
  if(voisin[W]==MPI_PROC_NULL){

 
  MPI_Sendrecv(&(U1_local_mat[sx][sy-1]),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(U1_local_mat[ex+1][sy-1]), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);
  
MPI_Sendrecv(&(U2_local_mat[sx][sy-1]),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(U2_local_mat[ex+1][sy-1]), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);
  
  }

  
   /**********************************************!!!!!!*****************/
  if(voisin[S]==MPI_PROC_NULL){

 
  MPI_Sendrecv(&(U1_local_mat[ex+1][sy]),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(U1_local_mat[ex+1][ey+1]), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat[ex+1][sy]),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(U2_local_mat[ex+1][ey+1]), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);
  
  }

  
  if(voisin[S]==MPI_PROC_NULL){

  
  MPI_Sendrecv(&(U1_local_mat[ex+1][ey]),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(U1_local_mat[ex+1][sy-1]), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);
 MPI_Sendrecv(&(U2_local_mat[ex+1][ey]),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(U2_local_mat[ex+1][sy-1]), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);
  


  }

 if(voisin[E]==MPI_PROC_NULL){

  
  MPI_Sendrecv(&(U1_local_mat[ex][ey+1]),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(U1_local_mat[sx-1][ey+1]), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat[ex][ey+1]),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(U2_local_mat[sx-1][ey+1]), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);

  }

  if(voisin[E]==MPI_PROC_NULL){

  
  MPI_Sendrecv(&(U1_local_mat[sx][ey+1]),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(U1_local_mat[ex+1][ey+1]), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);
 
MPI_Sendrecv(&(U2_local_mat[sx][ey+1]),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(U2_local_mat[ex+1][ey+1]), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);

  }
 
  

}


   void communication_interpolation(double **interpolant_local,double **dx_interpolant_local,double **dy_interpolant_local,int * tab_bounds,MPI_Comm comm2d,int * voisin) {

 
  /*****CrÃ©ation des types dÃ©rivÃ©s*****/
  MPI_Datatype type_ligne, type_colonne_mono;

  MPI_Type_contiguous(tab_bounds[3]-tab_bounds[2]+1,MPI_DOUBLE,&type_ligne);
  MPI_Type_commit(&type_ligne);
  
  MPI_Type_vector(tab_bounds[1]-tab_bounds[0]+1,1,tab_bounds[3]-tab_bounds[2]+3, MPI_DOUBLE, &type_colonne_mono);
  MPI_Type_commit(&type_colonne_mono); 
  /************************************/

  const int etiquette = 500;
  MPI_Status statut;
  int sx,ex,sy,ey;
  sx=1;
  sy=1;
  ex=tab_bounds[1]-tab_bounds[0]+1;
  ey=tab_bounds[3]-tab_bounds[2]+1;
 
  
  /* Envoi au voisin N et reception du voisin S */
  
  MPI_Sendrecv(&interpolant_local[sx][sy], 1, type_ligne, voisin[North], etiquette, 
	       &interpolant_local[ex+1][sy], 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  MPI_Sendrecv(&dx_interpolant_local[sx][sy], 1, type_ligne, voisin[North], etiquette, 
	       &dx_interpolant_local[ex+1][sy], 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut);

  MPI_Sendrecv(&dy_interpolant_local[sx][sy], 1, type_ligne, voisin[North], etiquette, 
	       &dy_interpolant_local[ex+1][sy], 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

 

  
  /* Envoi au voisin S et reception du voisin N */
  MPI_Sendrecv(&(interpolant_local[ex][sy]), 1, type_ligne, voisin[S], etiquette, 
	       &(interpolant_local[sx-1][sy]), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
  MPI_Sendrecv(&(dx_interpolant_local[ex][sy]), 1, type_ligne, voisin[S], etiquette, 
	       &(dx_interpolant_local[sx-1][sy]), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
  MPI_Sendrecv(&(dy_interpolant_local[ex][sy]), 1, type_ligne, voisin[S], etiquette, 
	       &(dy_interpolant_local[sx-1][sy]), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);

  

 
  /* Envoi au voisin W et  reception du voisin E */
 
  MPI_Sendrecv(&(interpolant_local[sx][sy]), 1, type_colonne_mono , voisin[W], etiquette, 
		&(interpolant_local[sx][ey+1]), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(dx_interpolant_local[sx][sy]), 1, type_colonne_mono , voisin[W], etiquette, 
		&(dx_interpolant_local[sx][ey+1]), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(dy_interpolant_local[sx][sy]), 1, type_colonne_mono , voisin[W], etiquette, 
		&(dy_interpolant_local[sx][ey+1]), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  
  

  /* Envoi au voisin E et  reception du voisin W */
 
  
  MPI_Sendrecv(&(interpolant_local[sx][ey]), 1, type_colonne_mono, voisin[E], etiquette, 
		&(interpolant_local[sx][sy-1]), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(dx_interpolant_local[sx][ey]), 1, type_colonne_mono, voisin[E], etiquette, 
		&(dx_interpolant_local[sx][sy-1]), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  MPI_Sendrecv(&(dy_interpolant_local[sx][ey]), 1, type_colonne_mono, voisin[E], etiquette, 
		&(dy_interpolant_local[sx][sy-1]), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  
  

}

   void communication_f(double **f_local,int * tab_bounds,MPI_Comm comm2d,int * voisin) {

 
  /*****CrÃ©ation des types dÃ©rivÃ©s*****/
  MPI_Datatype type_ligne, type_colonne_mono;

  MPI_Type_contiguous(tab_bounds[3]-tab_bounds[2]+1,MPI_DOUBLE,&type_ligne);
  MPI_Type_commit(&type_ligne);
  
  MPI_Type_vector(tab_bounds[1]-tab_bounds[0]+1,1,tab_bounds[3]-tab_bounds[2]+3, MPI_DOUBLE, &type_colonne_mono);
  MPI_Type_commit(&type_colonne_mono); 
  /************************************/

  const int etiquette = 900;
  MPI_Status statut;
  int sx,ex,sy,ey;
  sx=1;
  sy=1;
  ex=tab_bounds[1]-tab_bounds[0]+1;
  ey=tab_bounds[3]-tab_bounds[2]+1;
 
  
  /* Envoi au voisin N et reception du voisin S */
  
  MPI_Sendrecv(&f_local[sx][sy], 1, type_ligne, voisin[North], etiquette, 
	       &f_local[ex+1][sy], 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  
  /* Envoi au voisin S et reception du voisin N */
  MPI_Sendrecv(&(f_local[ex][sy]), 1, type_ligne, voisin[S], etiquette, 
	       &(f_local[sx-1][sy]), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
 
  /* Envoi au voisin W et  reception du voisin E */
 
  MPI_Sendrecv(&(f_local[sx][sy]), 1, type_colonne_mono , voisin[W], etiquette, 
		&(f_local[sx][ey+1]), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  
  

  /* Envoi au voisin E et  reception du voisin W */
 
  
  MPI_Sendrecv(&(f_local[sx][ey]), 1, type_colonne_mono, voisin[E], etiquette, 
		&(f_local[sx][sy-1]), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
 
  

}

void communication_Ttilde(double **Ttilde_local_extended,int * tab_bounds,MPI_Comm comm2d,int * voisin, int * voisin_diagonale, int iw, int iw2, int im,int im2) {

 
  /*****Création des types dérivés*****/
  MPI_Datatype type_colonne;

  
  MPI_Type_vector(tab_bounds[1]-tab_bounds[0]+1,iw+im2,tab_bounds[3]-tab_bounds[2]+1+2*(iw+im2), MPI_DOUBLE, &type_colonne);
  MPI_Type_commit(&type_colonne); 
 

  MPI_Datatype type_iwlignes;
  MPI_Type_vector(iw+im2,tab_bounds[3]-tab_bounds[2]+1,tab_bounds[3]-tab_bounds[2]+1+2*(iw+im2),MPI_DOUBLE,&type_iwlignes);
  MPI_Type_commit(&type_iwlignes);
  /************************************/

  const int etiquette = 100;
  MPI_Status statut;
  int sx,ex,sy,ey;
  sx=iw+im2;
  sy=iw+im2;
  ex=tab_bounds[1]-tab_bounds[0]+iw+im2;
  ey=tab_bounds[3]-tab_bounds[2]+iw+im2;
 
  
  /* Envoi au voisin N et reception du voisin S */


 MPI_Sendrecv(&(Ttilde_local_extended[sx][sy]),1,type_iwlignes,voisin[North],etiquette,&(Ttilde_local_extended[ex+1][sy]),1,type_iwlignes,voisin[S],etiquette,comm2d,&statut);
 /* OK*/
 
  /* Envoi au voisin S et reception du voisin N */

  MPI_Sendrecv(&(Ttilde_local_extended[ex-iw-im2+1][sy]),1,type_iwlignes,voisin[S],etiquette,&(Ttilde_local_extended[sx-iw-im2][sy]),1,type_iwlignes,voisin[North],etiquette,comm2d,&statut);
 
  /* Envoi au voisin W et  reception du voisin E */
  
  MPI_Sendrecv(&(Ttilde_local_extended[sx][sy]), 1, type_colonne , voisin[W], etiquette, 
		&(Ttilde_local_extended[sx][ey+1]), 1, type_colonne , voisin[E], etiquette, 
		comm2d, &statut);


  /* Envoi au voisin E et  reception du voisin W */

  MPI_Sendrecv(&(Ttilde_local_extended[sx][ey-iw-im2+1]), 1, type_colonne, voisin[E], etiquette, 
		&(Ttilde_local_extended[sx][sy-iw-im2]), 1, type_colonne, voisin[W], etiquette, 
		comm2d, &statut);

  
  /*********************************************************************/
  /**************************Gestion des termes diagonaux***************/
  /*********************************************************************/


   /*Création du type subarray*/
  int dimensions_sub[2], dimensions_big[2];
  //Reception
  MPI_Datatype RsubNE, RsubSO, RsubON, RsubES;//Reception
  int RcoordsNE[2], RcoordsSO[2], RcoordsON[2], RcoordsES[2];
  //Envoi
  MPI_Datatype EsubNE, EsubSO, EsubON, EsubES;//Envoi
  int EcoordsNE[2], EcoordsSO[2], EcoordsON[2], EcoordsES[2];

  dimensions_big[1] = tab_bounds[3]-tab_bounds[2]+1+2*(iw+im2);
  dimensions_big[0] = tab_bounds[1]-tab_bounds[0]+1+2*(iw+im2);
  dimensions_sub[0] = dimensions_sub[1] = iw+im2;

  EcoordsON[0] =  sx;   EcoordsON[1] = sy;
  EcoordsSO[0] = ex-iw-im2+1; EcoordsSO[1] = sy;
  EcoordsNE[0] = sx;    EcoordsNE[1] = ey-iw-im2+1;
  EcoordsES[0] = ex-iw-im2+1;  EcoordsES[1] = ey-iw-im2+1;

  RcoordsON[0] = sx-iw-im2;  RcoordsON[1] =sy-iw-im2;
  RcoordsSO[0] = ex+1;  RcoordsSO[1] = sy-iw-im2;
  RcoordsNE[0] = sx-iw-im2;  RcoordsNE[1] = ey+1; 
  RcoordsES[0] = ex+1;  RcoordsES[1] = ey+1;

 

  
  MPI_Type_create_subarray(2,dimensions_big,dimensions_sub,RcoordsON,MPI_ORDER_C,MPI_DOUBLE,&RsubON);
  MPI_Type_create_subarray(2,dimensions_big,dimensions_sub,EcoordsON,MPI_ORDER_C,MPI_DOUBLE,&EsubON);

  MPI_Type_create_subarray(2,dimensions_big,dimensions_sub,RcoordsES,MPI_ORDER_C,MPI_DOUBLE,&RsubES);
  MPI_Type_create_subarray(2,dimensions_big,dimensions_sub,EcoordsES,MPI_ORDER_C,MPI_DOUBLE,&EsubES);

  MPI_Type_create_subarray(2,dimensions_big,dimensions_sub,RcoordsNE,MPI_ORDER_C,MPI_DOUBLE,&RsubNE);
  MPI_Type_create_subarray(2,dimensions_big,dimensions_sub,EcoordsNE,MPI_ORDER_C,MPI_DOUBLE,&EsubNE);

  MPI_Type_create_subarray(2,dimensions_big,dimensions_sub,RcoordsSO,MPI_ORDER_C,MPI_DOUBLE,&RsubSO);
  MPI_Type_create_subarray(2,dimensions_big,dimensions_sub,EcoordsSO,MPI_ORDER_C,MPI_DOUBLE,&EsubSO);

  MPI_Type_commit(&RsubON);
  MPI_Type_commit(&EsubON);
  MPI_Type_commit(&RsubSO);
  MPI_Type_commit(&EsubSO);
  MPI_Type_commit(&RsubNE);
  MPI_Type_commit(&EsubNE);
  MPI_Type_commit(&RsubES);
  MPI_Type_commit(&EsubES);





  /*Envoi au voisin NE et reception du voisin SO*/

  
   MPI_Sendrecv(&(Ttilde_local_extended[0][0]),1,EsubNE,voisin_diagonale[NE],etiquette,&(Ttilde_local_extended[0][0]),1,RsubSO,voisin_diagonale[SO],etiquette,comm2d,&statut);

  /*Envoi au voisin SO et reception du voisin NE*/

   MPI_Sendrecv(&(Ttilde_local_extended[0][0]),1,EsubSO,voisin_diagonale[SO],etiquette,&(Ttilde_local_extended[0][0]),1,RsubNE,voisin_diagonale[NE],etiquette,comm2d,&statut);


  /*Envoi au voisin ES et reception du voisin ON*/

   MPI_Sendrecv(&(Ttilde_local_extended[0][0]),1,EsubES,voisin_diagonale[ES],etiquette,&(Ttilde_local_extended[0][0]),1,RsubON,voisin_diagonale[ON],etiquette,comm2d,&statut);


  /*Envoi au voisin ON et reception du voisin ES*/

   MPI_Sendrecv(&(Ttilde_local_extended[0][0]),1,EsubON,voisin_diagonale[ON],etiquette,&(Ttilde_local_extended[0][0]),1,RsubES,voisin_diagonale[ES],etiquette,comm2d,&statut);



  MPI_Type_free(&type_iwlignes);
  MPI_Type_free(&type_colonne);
  MPI_Type_free(&EsubON);
  MPI_Type_free(&EsubES);
  MPI_Type_free(&EsubSO);
  MPI_Type_free(&EsubNE);
  MPI_Type_free(&RsubON);
  MPI_Type_free(&RsubES);
  MPI_Type_free(&RsubSO);
  MPI_Type_free(&RsubNE); 


}
