#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <mpi.h>

#ifdef MKL_BLAS
#include <mkl_cblas.h>
#include <mkl_lapack.h>
#else
#include <cblas.h>
#endif

#include "params.h"
#include "functions.h"
#include "dimens.h"

void resolution(double** R_local_mat,double ** g_local_extended,int M_R, int N_R, double** U1_local_mat, double** U2_local_mat,double* restrict * V11_local_mat,double* restrict * V12_local_mat,double* restrict * V21_local_mat,double* restrict * V22_local_mat,double** U1_local_mat_new, double** U2_local_mat_new,double* restrict * V11_local_mat_new,double* restrict * V12_local_mat_new,double* restrict * V21_local_mat_new,double* restrict * V22_local_mat_new,double * restrict * restrict Ttilde_local_extended,double * restrict * restrict Ttilde_local_extended_new, int * tab_bounds,int * voisin,int * voisin_diagonale,int rang,double theta_lip,int ntx,int nty,MPI_Comm comm2d,int im,int iw,double *** pfW,int *** piY,int ** piSizeNeigh){
		
 
	int i,j,k,l,i1; 
        int ix,iy;
        int i_inter;
        double intermediate_value1,intermediate_value2; 
	double c1;	//Moyenne de R a l'interieur du contour de Ttilde pour Chan-Vese
	double c2;
	int nb;
	double c0_U1=1.0+dt*4.0*epsilon2;
	double c1_global;
        double c2_global; 
        int nb_global;
        int iw2=(iw-1)/2;
        int im2=(im-1)/2;
        int isqw2 = iw*iw;  
        double fNormU,fGuij,fSum1,fw;
        int iNbNeighX;
	/* *********************************************/
	/* coefficients Ã©lasticitÃ© non linÃ©aire        */
	/* *********************************************/

	double a1= -(coeff_lame_lambda+coeff_lame_mu)/2.0;
	double a2= (coeff_lame_lambda+2.0*coeff_lame_mu)/8.0; //Coefficient beta du papier dans la fonction de densitÃ© quasiconvexifiÃ©e
	double alpha_q=2.0*(coeff_lame_lambda+coeff_lame_mu)/(coeff_lame_lambda+2.0*coeff_lame_mu);//Coefficient alpha du papier dans la fonction de densitÃ© quasiconvexifiÃ©e
	double d;
	double H,dir,coeff2,coeff4,detV_2,c01;
	c01=1.0+dt*epsilon2;
        double gradient2,gradient1;
        double divergence,divergence_plusi,divergence_plusj,norme; 


        double ** interpolant_local=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
        init_mat(interpolant_local,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
        double ** dx_interpolant_local=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
        init_mat(dx_interpolant_local,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
        double ** dy_interpolant_local=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
        init_mat(dy_interpolant_local,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
        double ** Id1U1_local=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
        double ** Id2U2_local=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
 
       
        double ** C=allocarray(M_R,N_R);
  
        double ** fNorm=allocarray((tab_bounds[1]-tab_bounds[0]+1+2*iw2),(tab_bounds[3]-tab_bounds[2]+1+2*iw2));
        
        

        int ** Id1=allocarray_int(M_R,N_R); /*direction des colonnes/direction des x*/ //-->Global
        int ** Id2=allocarray_int(M_R,N_R); /*direction des lignes/direction des y*/ //-->Global
        if(rang==0){
        for(i=0;i<M_R;i++){
		for(j=0;j<N_R;j++){
			Id1[i][j]=j+1;
			Id2[i][j]=i+1;
		}
	}
        }

        MPI_Bcast(&(Id1[0][0]),M_R*N_R,MPI_INT,0,comm2d); 
        MPI_Bcast(&(Id2[0][0]),M_R*N_R,MPI_INT,0,comm2d);

      
        /*Variable auxiliaire pour la projection de Chambolle*/
        double ** f_local=allocarray((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
        init_mat(f_local,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
      

        /*Déclaration des éléments dont on a besoin*/
        
        FILE *data_T;
        double * T_global_vect;
        double ** T_global_mat;
        

        if(rang==0){
        
        T_global_vect=malloc(M_R*N_R*sizeof(double));
        T_global_mat=allocarray(M_R,N_R);
        data_T = fopen("Template01_img2.bin","r");
        fread(T_global_vect,sizeof(T_global_vect),N_R*M_R,data_T);
        fclose(data_T);
        	for (i=0;i<M_R;i++) {
	        T_global_mat[i] = &(T_global_vect[i*N_R]);
                }
       
        coeff_spline_multiscale(M_R,N_R,T_global_mat,C,theta_lip);


        }
        /*Diffusion de C à l'ensemble des processeurs*/

        MPI_Bcast(&(C[0][0]),M_R*N_R,MPI_DOUBLE,0,comm2d);
         
        /*****************Interpolation effectuée localement par les processeurs********/ 
        /*Création de Id1U1_local*/
        /*Création de Id2U2_local*/
        for(int i=0;i<(tab_bounds[1]-tab_bounds[0]+3);i++){

                     for(int j=0;j<(tab_bounds[3]-tab_bounds[2]+3);j++){
 
                     Id1U1_local[i][j]=1.0*Id1[tab_bounds[0]-1+i][tab_bounds[2]-1+j]+U1_local_mat[i][j];
                     Id2U2_local[i][j]=1.0*Id2[tab_bounds[0]-1+i][tab_bounds[2]-1+j]+U2_local_mat[i][j];
                     }


        }


        /*Processus d'interpolation réalisé localement par chacun des processeurs*/ 
        
        interpol2D((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3),Id2U2_local,Id1U1_local,C,interpolant_local,dx_interpolant_local,dy_interpolant_local,M_R,N_R); 
       
 
      
        /* ************************** */
	/* **** BOUCLE RESOLUTION ****/
	/* ************************* */

	
        for(k=0;k<IterMax;k++){
			if(rang==0){
			printf("iteration par rapport au temps : %d \n",k);
                        }
			
                        
                        

			c1=0.0;
                        c1_global=0.0;
			c2=0.0;
                        c2_global=0.0;
			nb=0;
                        nb_global=0; 


			for(i=1;i<(tab_bounds[1]-tab_bounds[0]+2);i++){// Parce qu'il y a les cellules fantÃ´mes
#pragma omp simd reduction(+:c1,c2,nb)
				for(j=1;j<(tab_bounds[3]-tab_bounds[2]+2);j++){// Parce qu'il y a les cellules fantÃ´mes

					detV_2=(1.0+V11_local_mat[i][j])*(1.0+V22_local_mat[i][j])-V12_local_mat[i][j]*V21_local_mat[i][j]-2.0;
					coeff2=(1.0+V11_local_mat[i][j])*(1.0+V11_local_mat[i][j])+V12_local_mat[i][j]*V12_local_mat[i][j]+V21_local_mat[i][j]*V21_local_mat[i][j]+(1.0+V22_local_mat[i][j])*(1.0+V22_local_mat[i][j])-alpha_q;
					H=heaviside(coeff2,eps);
					dir=dirac(coeff2,eps);
					coeff4=a2*coeff2*coeff2;

					
					V11_local_mat_new[i][j]=(1.0/c01)*(V11_local_mat[i][j]+dt*(-coeff_lame_mu*detV_2*(1.0+V22_local_mat[i][j])
								-4.0*a2*(1.0+V11_local_mat[i][j])*coeff2*H
								-2.0*dir*coeff4*(1.0+V11_local_mat[i][j])+0.5*epsilon2*(U1_local_mat[i][j+1]-U1_local_mat[i][j-1])));


					V12_local_mat_new[i][j]=(1.0/c01)*(V12_local_mat[i][j]+dt*(coeff_lame_mu*detV_2*V21_local_mat[i][j]
								-4.0*a2*V12_local_mat[i][j]*coeff2*H
								-2.0*dir*coeff4*V12_local_mat[i][j]+0.5*epsilon2*(U1_local_mat[i+1][j]-U1_local_mat[i-1][j])));

					V21_local_mat_new[i][j]=(1.0/c01)*(V21_local_mat[i][j]+dt*(coeff_lame_mu*detV_2*V12_local_mat[i][j]
								-4.0*a2*V21_local_mat[i][j]*coeff2*H
								-2.0*dir*coeff4*V21_local_mat[i][j]+0.5*epsilon2*(U2_local_mat[i][j+1]-U2_local_mat[i][j-1])));

					V22_local_mat_new[i][j]=(1.0/c01)*(V22_local_mat[i][j]+dt*(-coeff_lame_mu*detV_2*(1.0+V11_local_mat[i][j])
								-4.0*a2*(1.0+V22_local_mat[i][j])*coeff2*H
								-2.0*dir*coeff4*(1.0+V22_local_mat[i][j])+0.5*epsilon2*(U2_local_mat[i+1][j]-U2_local_mat[i-1][j])));

                                        
					if(Ttilde_local_extended[i+iw+im2-1][j+iw+im2-1]>rho){
						c1=c1+R_local_mat[i][j];
                                                
						nb=nb+1;
					}
					else c2=c2+R_local_mat[i][j];

								
				}
			}
                        
                        /*copie_mat2D(V11_local_mat,V11_local_mat_new,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
                        copie_mat2D(V12_local_mat,V12_local_mat_new,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
                        copie_mat2D(V21_local_mat,V21_local_mat_new,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));
                        copie_mat2D(V22_local_mat,V22_local_mat_new,(tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3));*/
			double * tmp;
			for (i = 0; i < (tab_bounds[1]-tab_bounds[0]+3); ++i) {
				tmp = V11_local_mat[i];
				V11_local_mat[i] = V11_local_mat_new[i];
				V11_local_mat_new[i] = tmp;
			}
			for (i = 0; i < (tab_bounds[1]-tab_bounds[0]+3); ++i) {
				tmp = V12_local_mat[i];
				V12_local_mat[i] = V12_local_mat_new[i];
				V12_local_mat_new[i] = tmp;
			}
			for (i = 0; i < (tab_bounds[1]-tab_bounds[0]+3); ++i) {
				tmp = V11_local_mat[i];
				V21_local_mat[i] = V21_local_mat_new[i];
				V21_local_mat_new[i] = tmp;
			}
			for (i = 0; i < (tab_bounds[1]-tab_bounds[0]+3); ++i) {
				tmp = V11_local_mat[i];
				V22_local_mat[i] = V22_local_mat_new[i];
				V22_local_mat_new[i] = tmp;
			}
                        
                        communication_V(V11_local_mat,V12_local_mat,V21_local_mat,V22_local_mat,tab_bounds,comm2d,voisin);
                     
                       

                        MPI_Allreduce(&c1,&c1_global,1,MPI_DOUBLE,MPI_SUM,comm2d);
                        MPI_Allreduce(&c2,&c2_global,1,MPI_DOUBLE,MPI_SUM,comm2d);
                        MPI_Allreduce(&nb,&nb_global,1,MPI_INT,MPI_SUM,comm2d);
                       
                        c1_global=c1_global/(double)(nb_global);
			c2_global=c2_global/(double)((M_R-2)*(N_R-2)-nb_global);
                       

                        for(int i_inter=0;i_inter<n_max;i_inter++){

			for(i=1;i<(tab_bounds[1]-tab_bounds[0]+2);i++){// Parce qu'il y a les cellules fantÃ´mes
#pragma omp simd        
				for(j=1;j<(tab_bounds[3]-tab_bounds[2]+2);j++){// Parce qu'il y a les cellules fantÃ´mes

					
					
					U1_local_mat_new[i][j]=(1.0/c0_U1)*(U1_local_mat[i][j]+dt*epsilon2*(U1_local_mat[i+1][j]+U1_local_mat[i-1][j]+U1_local_mat[i][j+1]+U1_local_mat[i][j-1])+dt*(-dx_interpolant_local[i][j]*255.0*255.0*(interpolant_local[i][j]-f_local[i][j]-Ttilde_local_extended[i-1+iw+im2][j-1+iw+im2])/theta-lambdanl*dx_interpolant_local[i][j]*255.0*255.0*(interpolant_local[i][j]-R_local_mat[i][j])-epsilon2*0.5*(V11_local_mat[i][j+1]-V11_local_mat[i][j-1])-epsilon2*0.5*(V12_local_mat[i+1][j]-V12_local_mat[i-1][j])));



					
					U2_local_mat_new[i][j]=(1.0/c0_U1)*(U2_local_mat[i][j]+dt*epsilon2*(U2_local_mat[i+1][j]+U2_local_mat[i-1][j]+U2_local_mat[i][j+1]+U2_local_mat[i][j-1])+dt*(-dy_interpolant_local[i][j]*255.0*255.0*(interpolant_local[i][j]-f_local[i][j]-Ttilde_local_extended[i-1+iw+im2][j-1+iw+im2])/theta-lambdanl*dy_interpolant_local[i][j]*255.0*255.0*(interpolant_local[i][j]-R_local_mat[i][j])-epsilon2*0.5*(V21_local_mat[i][j+1]-V21_local_mat[i][j-1])-epsilon2*0.5*(V22_local_mat[i+1][j]-V22_local_mat[i-1][j])));
				}
			}
                        
                         for(i=1;i<(tab_bounds[1]-tab_bounds[0]+2);i++){// Parce qu'il y a les cellules fantÃ´mes
#pragma omp simd
				        for(j=1;j<(tab_bounds[3]-tab_bounds[2]+2);j++){// Parce qu'il y a les cellules fantÃ´mes

						U1_local_mat[i][j]=U1_local_mat_new[i][j];
						U2_local_mat[i][j]=U2_local_mat_new[i][j];
					}
				}
                         
                         communication(U1_local_mat,U2_local_mat,tab_bounds,comm2d,voisin,voisin_diagonale);
                         
        
                        }// fin de la boucle en i_inter
        

                         /*Actualisation de l'interpolant*/
                     
        for(int i=0;i<(tab_bounds[1]-tab_bounds[0]+3);i++){

                     for(int j=0;j<(tab_bounds[3]-tab_bounds[2]+3);j++){
 
                     Id1U1_local[i][j]=1.0*Id1[tab_bounds[0]-1+i][tab_bounds[2]-1+j]+U1_local_mat[i][j];
                     Id2U2_local[i][j]=1.0*Id2[tab_bounds[0]-1+i][tab_bounds[2]-1+j]+U2_local_mat[i][j];
                     }


        }


        /*Processus d'interpolation réalisé localement par chacun des processeurs*/ 
        
        interpol2D((tab_bounds[1]-tab_bounds[0]+3),(tab_bounds[3]-tab_bounds[2]+3),Id2U2_local,Id1U1_local,C,interpolant_local,dx_interpolant_local,dy_interpolant_local,M_R,N_R); 
                         

   /*PARTIE DELICATE -->ACTUALISATION DE Ttilde */
   //double *** pfW,int *** piY,int ** piSizeNeigh

      for(int i=0;i<(tab_bounds[1]-tab_bounds[0]+1+2*iw2);i++){ 

                     for(int j=0;j<(tab_bounds[3]-tab_bounds[2]+1+2*iw2);j++){ 
 
                     fNormU = 0.0;
            	     iNbNeighX = piSizeNeigh[i][j];
            		       for (i1=0; i1<iNbNeighX; i1++)
            					{
                					
                					fw=pfW[i][j][i1];//poids du voisin courant
                					ix=piY[i][j][2*i1];// coordonnées dans la direction des colonnes du voisin 
                                                        iy=piY[i][j][2*i1+1]; 
                                                       


                                                       
                					fGuij = fw* (Ttilde_local_extended[iy][ix]-Ttilde_local_extended[i+iw+im2-iw2][j+iw+im2-iw2])*(Ttilde_local_extended[iy][ix]-Ttilde_local_extended[i+iw+im2-iw2][j+iw+im2-iw2]);
                					fNormU += fGuij; 

            					}
                                              
            					fNorm[i][j]=sqrt(fNormU);
                                              
            	      }
	}


  
    for(int i=0;i<(tab_bounds[1]-tab_bounds[0]+1);i++){ //indexation dans Ttilde   i+iw+im2

                     for(int j=0;j<(tab_bounds[3]-tab_bounds[2]+1);j++){ //indexation dans Ttilde j+iw+im2

                				double reach_max=0.1;
                                                double reach_max0=0.1;
                                                double reach_min=0.1;
                                                double reach_min0=0.1;
                				fSum1 = 0.0;
                				iNbNeighX = piSizeNeigh[i+iw2][j+iw2];
                				for (i1=0; i1<iNbNeighX; i1++)
                				{
                    					ix=piY[i+iw2][j+iw2][2*i1]; 
                                                        iy=piY[i+iw2][j+iw2][2*i1+1]; 
                    					fw=pfW[i+iw2][j+iw2][i1];
                                                    
                                                       
                                                        intermediate_value1=g_local_extended[iy][ix]/(2.0*fNorm[iy-iw-im2+iw2][ix-iw-im2+iw2]+0.05);

                                                         
                                                       intermediate_value2=g_local_extended[i+iw+im2][j+iw+im2]/(2.0*fNorm[i+iw2][j+iw2]+0.05);

                    					fSum1 +=dt*fw*(Ttilde_local_extended[iy][ix]-Ttilde_local_extended[i+iw+im2][j+iw+im2])*(intermediate_value1+intermediate_value2);
							
                                              }
                                                

                				Ttilde_local_extended_new[i+iw+im2][j+iw+im2]=Ttilde_local_extended[i+iw+im2][j+iw+im2]+fSum1-dt/theta*(Ttilde_local_extended[i+iw+im2][j+iw+im2]-interpolant_local[i+1][j+1]+f_local[i+1][j+1])-dt*lambdal2*(c1_global-R_local_mat[i+1][j+1])*(c1_global-R_local_mat[i+1][j+1])+dt*lambdal2*(c2_global-R_local_mat[i+1][j+1])*(c2_global-R_local_mat[i+1][j+1]);
						

                			}
				}
			
         for(int i=0;i<(tab_bounds[1]-tab_bounds[0]+1);i++){ //indexation dans Ttilde   i+iw+im2
#pragma omp simd        

                     for(int j=0;j<(tab_bounds[3]-tab_bounds[2]+1);j++){ //indexation dans Ttilde j+iw+im2


                                              Ttilde_local_extended[i+iw+im2][j+iw+im2]=Ttilde_local_extended_new[i+iw+im2][j+iw+im2];
                     }
         }
         


     communication_Ttilde(Ttilde_local_extended,tab_bounds,comm2d,voisin,voisin_diagonale,iw,iw2,im,im2);
	//if(rang==0)printarr(Ttilde_local_extended,(tab_bounds[1]-tab_bounds[0]+1+2*(iw+im2)),(tab_bounds[3]-tab_bounds[2]+1+2*(iw+im2)),"verif");			
         
        //MPI_Abort(comm2d,56);
                    for(i=1;i<(tab_bounds[1]-tab_bounds[0]+2);i++){// Parce qu'il y a les cellules fantÃ´mes
				for(j=1;j<(tab_bounds[3]-tab_bounds[2]+2);j++){// Parce qu'il y a les cellules fantÃ´mes
						
                                               

						if(interpolant_local[i][j]-Ttilde_local_extended[i+iw+im2-1][j+iw+im2-1]>=theta*epsilon1)
							f_local[i][j]=interpolant_local[i][j]-Ttilde_local_extended[i+iw+im2-1][j+iw+im2-1]-theta*epsilon1;
						else if(interpolant_local[i][j]-Ttilde_local_extended[i+iw+im2-1][j+iw+im2-1]<=-theta*epsilon1)
							f_local[i][j]=interpolant_local[i][j]-Ttilde_local_extended[i+iw+im2-1][j+iw+im2-1]+theta*epsilon1;
						else
							f_local[i][j]=0.0;

					}
				}

  	                      
                              communication_f(f_local,tab_bounds,comm2d,voisin);   
                       		
                              
       		
       
       

    }//fin de la boucle k
   
 
    
    double * interpolant_local_vect=malloc((tab_bounds[1]-tab_bounds[0]+3)*(tab_bounds[3]-tab_bounds[2]+3));   
    interpolant_local_vect=*interpolant_local;//interpolant_local;      
    ecrire_mpi(interpolant_local_vect,ntx,nty,tab_bounds,comm2d,"final_interpolant_nonlocal_V2_8.bin"); 

    /*double ** Ttilde_local_resized=allocarray(tab_bounds[1]-tab_bounds[0]+1,tab_bounds[3]-tab_bounds[2]+1);

    for(i=0;i<tab_bounds[1]-tab_bounds[0]+1;i++){
			for(j=0;j<tab_bounds[3]-tab_bounds[2]+1;j++){

                        Ttilde_local_resized[i][j]=Ttilde_local_extended[i+iw+im2][j+iw+im2];
                        printf("%lf\n",Ttilde_local_resized[i][j]); 
                        }
    }

    FILE * final_Ttilde;
                final_Ttilde=fopen("checking_Ttilde.txt","w"); 

		for(i=0;i<tab_bounds[1]-tab_bounds[0]+1;i++){
			for(j=0;j<tab_bounds[3]-tab_bounds[2]+1;j++){
				fprintf(final_Ttilde,"%lf\t",Ttilde_local_resized[i][j]);
			}
			fprintf(final_Ttilde,"\n");
		}
		fclose(final_Ttilde);*/

       
printf("MIRACLE\n");        
}
#define M_PI 3.14159265359

// calcul la fonction regularisee de Heaviside 
double heaviside(double phi,double eps){

	return(0.5*(1.0+(2.0/M_PI)*atan(phi/eps)));
}

//dirac regularisee
double dirac(double phi,double eps){

	return((eps/M_PI)/(eps*eps+phi*phi));
}


void coeff_spline_multiscale(int M, int N,double ** T,double ** C, double theta_lip){


	double ** Bm;
	double ** Bn;
	double ** IN;
	double ** IM;
	double ** Dn;
	double ** Dm;


	

	int i,j;


	Bm=(double**)calloc(N,sizeof(double *));
	Dm=(double**)calloc(N-1,sizeof(double *));
	IN=(double**)calloc(N,sizeof(double *));

	Bn=(double**)calloc(M,sizeof(double *));
	Dn=(double**)calloc(M-1,sizeof(double *));
	IM=(double**)calloc(M,sizeof(double *));



	for(i=0;i<N;i++){

		Bm[i]=(double*)calloc(N,sizeof(double));
		IN[i]=(double*)calloc(N,sizeof(double));
	}
	for(i=0;i<N-1;i++){
		Dm[i]=(double*)calloc(N,sizeof(double));
	}

	for(i=0;i<M;i++){

		Bn[i]=(double*)calloc(M,sizeof(double));
		IM[i]=(double*)calloc(M,sizeof(double));
	}
	for(i=0;i<M-1;i++){
		Dn[i]=(double*)calloc(M,sizeof(double));
	}

	Bm[0][0]=4.0;
	Bm[0][1]=1.0;
	IN[0][0]=1.0;
	IM[0][0]=1.0;

	for(i=1;i<N-1;i++){
		Bm[i][i]=4.0;
		Bm[i][i-1]=1.0;
		Bm[i][i+1]=1.0;
		IN[i][i]=1.0;

	}

	Bm[N-1][N-1]=4.0;
	Bm[N-1][N-2]=1.0;
	IN[N-1][N-1]=1.0;
	Bn[0][0]=4.0;
	Bn[0][1]=1.0;
	for(i=1;i<M-1;i++){

		Bn[i][i]=4.0;
		Bn[i][i-1]=1.0;
		Bn[i][i+1]=1.0;
		IM[i][i]=1.0;

	}
	Bn[M-1][M-1]=4.0;
	Bn[M-1][M-2]=1.0;
	IM[M-1][M-1]=1.0;

	for(i=0;i<N-1;i++){
		Dm[i][i]=-1.0;
		Dm[i][i+1]=1.0;
	}
	for(i=0;i<M-1;i++){
		Dn[i][i]=-1.0;
		Dn[i][i+1]=1.0;
	}


	double * Bm_vec;
	double * Dm_vec;
	double * Wm_vec;
	double * Bn_vec;
	double * Dn_vec;
	double * Wn_vec;
	double * IN_vec;
	double * IM_vec;

	Bm_vec=(double*)calloc(N*N,sizeof(double));
	Dm_vec=(double*)calloc(N*(N-1),sizeof(double));
	Wm_vec=(double*)calloc(N*N,sizeof(double));
	IN_vec=(double*)calloc(N*N,sizeof(double));
	Bn_vec=(double*)calloc(M*M,sizeof(double));
	Dn_vec=(double*)calloc(M*(M-1),sizeof(double));
	Wn_vec=(double*)calloc(M*M,sizeof(double));
	IM_vec=(double*)calloc(M*M,sizeof(double));



	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			Bm_vec[i*N+j]=Bm[i][j];
			IN_vec[i*N+j]=IN[i][j];
		}
	}

	for(i=0;i<M;i++){
		for(j=0;j<M;j++){
			Bn_vec[i*M+j]=Bn[i][j];
			IM_vec[i*M+j]=IM[i][j];
		}
	}

	for(i=0;i<N-1;i++){
		for(j=0;j<N;j++){
			Dm_vec[i*N+j]=Dm[i][j];
		}
	}


	for(i=0;i<M-1;i++){
		for(j=0;j<M;j++){
			Dn_vec[i*M+j]=Dn[i][j];
		}
	}

	double alpha;
	double beta;
	beta=0.0;
	alpha=theta_lip;


	cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,N,N,N-1,alpha,IN_vec,N,IN_vec,N,beta,Wm_vec,N);
	cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,M,M,M-1,alpha,IM_vec,M,IM_vec,M,beta,Wn_vec,M);
	/* contient la matrice In*/
	/*contient la matrice Im */

	beta=1.0;
	alpha=1.0;

	cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,N,N,N,alpha,Bm_vec,N,Bm_vec,N,beta,Wm_vec,N);
	cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,M,M,M,alpha,Bn_vec,M,Bn_vec,M,beta,Wn_vec,M);
	/* Wn_vec contient la matrice Mn*/
	/*Wm_vec contient la matrice Mm*/



	int info,lwork;
	double wkopt;
	double * work;
	int dim=N;
	int ipiv[dim];
	int nrhs=N;
	lwork=-1;
	dsysv_("lower",&dim,&nrhs,Wm_vec,&dim,ipiv,IN_vec,&dim,&wkopt,&lwork,&info);
	lwork=(int)wkopt;
	work=(double*)malloc(lwork*sizeof(double));
	dsysv_("lower",&dim,&nrhs,Wm_vec,&dim,ipiv,IN_vec,&dim,work,&lwork,&info);

	/* IN_vec contient la matrice inverse de M_m=Bm^T*Bm+theta*Dn^T*Dn*/

	double * T_vec;
	T_vec=(double*)calloc(M*N,sizeof(double));

	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			T_vec[i*N+j]=T[i][j];
		}
	}

	beta=0.0;
	alpha=1.0;
	double * Bn_dataT_vec;
	Bn_dataT_vec=(double*)calloc(M*N,sizeof(double));
	cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,M,N,M,alpha,Bn_vec,M,T_vec,N,beta,Bn_dataT_vec,N);


	double * Bn_dataT_Bm_vec;
	Bn_dataT_Bm_vec=(double*)calloc(M*N,sizeof(double));

	cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,M,N,N,alpha,Bn_dataT_vec,N,Bm_vec,N,beta,Bn_dataT_Bm_vec,N);

	double * Bn_dataT_Bm_invMm_vec;
	Bn_dataT_Bm_invMm_vec=(double*)calloc(M*N,sizeof(double));

	cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,M,N,N,alpha,Bn_dataT_Bm_vec,N,IN_vec,N,beta,Bn_dataT_Bm_invMm_vec,N);

	/* Bn_dataT_Bm_invMm_vec contient la matrice Bn*data_T*Bm*Mm^(-1) */


	int info1,lwork1;
	double wkopt1;
	double * work1;
	int dim1=M;
	int ipiv1[dim1];
	int nrhs1=M;
	lwork1=-1;

	dsysv_("lower",&dim1,&nrhs1,Wn_vec,&dim1,ipiv1,IM_vec,&dim1,&wkopt1,&lwork1,&info1);
	lwork1=(int)wkopt1;
	work1=(double*)malloc(lwork1*sizeof(double));
	dsysv_("lower",&dim1,&nrhs1,Wn_vec,&dim1,ipiv1,IM_vec,&dim1,work1,&lwork1,&info1);

	/*IM_vec contient la matrice inverse de Mn*/

	beta=0.0;
	alpha=1.0;
	double * C_vec;
	C_vec=(double*)calloc(M*N,sizeof(double));
	cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,M,N,M,alpha,IM_vec,M,Bn_dataT_Bm_invMm_vec,N,beta,C_vec,N);




	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			C[i][j]=C_vec[i*N+j];
		}
	}

	


	for(i=0;i<N;i++){
		free(Bm[i]);
		free(IN[i]);
	}
	for(i=0;i<N-1;i++){
		free(Dm[i]);
	}
	for(i=0;i<M-1;i++){
		free(Dn[i]);
	}
	for(i=0;i<M;i++){
		free(Bn[i]);
		free(IM[i]);
	}
	free(Bm),free(IN),free(Bn),free(IM),free(Dm),free(Dn);
	free(Bm_vec),free(Bn_vec),free(IN_vec),free(IM_vec),free(work),free(work1),free(T_vec),free(Bn_dataT_vec);
	free(Dm_vec),free(Wm_vec),free(Dn_vec),free(Wn_vec),free(Bn_dataT_Bm_vec),free(Bn_dataT_Bm_invMm_vec),free(C_vec);
}
