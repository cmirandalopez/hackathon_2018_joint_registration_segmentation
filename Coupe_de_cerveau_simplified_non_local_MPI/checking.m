clear all
close all
clc

fid=fopen('final_interpolant_nonlocal_V2_8.bin','r','l');
mydata=fread(fid,'double');
for(i=1:178)
    for(j=1:148)
        seq(i,j)=mydata((i-1)*148+j);
    end
end
figure;imshow(seq,[]);


Reference=imread('image1.pgm');

Reference_resized=Reference(2:end-1,2:end-1);
figure;imshow(Reference_resized);Reference=imread('image1.pgm');

Template=imread('image2.pgm');
Template_resized=Template(2:end-1,2:end-1);
figure;imshow(Template_resized);

fid=fopen('edge_detector.bin','r','l');
mydata=fread(fid,'double');
for(i=1:180)
    for(j=1:150)
        seq(i,j)=mydata((i-1)*150+j);
    end
end
figure;imshow(seq,[]);
