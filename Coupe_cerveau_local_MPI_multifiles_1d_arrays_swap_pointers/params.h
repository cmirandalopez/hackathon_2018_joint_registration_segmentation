
//const int IterMax=500;
const int IterMax=50;
//
//
//double seuil=0.0;  //pour la fonction sign image en 0-255 ou 0-1. USELESS
//
///*********Parameters for Chambolle's projection*******/
const int n_max=150; //nb iterations projection
const double theta=1.0;//parameter for dedoublement norme L1
const double tau=1.0/8.0;// projection step
//
///*****Parameter related to the Decoupled Problem*****/
const double epsilon1=0.1;
const double epsilon2=80000.0; /*appearing in the splitting part*/
//
const double c=10.0;//parameter appearing in the edge detector function
//
//
const double lambdal2=1.0;
const double lambdanl=1.0;/*L2 data fidelity term*/
//
///*****Coefficients related to the material*****/
const double coeff_lame_lambda=10.0;
const double coeff_lame_mu=1000.0;
//
//
const double dt=0.01; //pas de temps
//const double h=1.0;
const double eps=1.0;
const double tol=0.075;
const double rho=0.1;
