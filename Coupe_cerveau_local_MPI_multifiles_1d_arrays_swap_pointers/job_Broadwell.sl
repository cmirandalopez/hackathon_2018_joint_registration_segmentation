#!/bin/bash

# Slurm submission script, 
# MPI job with Intel MPI/srun
# CRIHAN v 1.00 - Jan 2017 
# support@criann.fr

#####SBATCH --reservation hak_broadwell

# Not shared resources
#SBATCH --exclusive

# Job name
#SBATCH -J "Graph_plot"

# Batch output file
#SBATCH --output broadwell.o%J

# Batch error file
#SBATCH --error broadwell.e%J

# Partition (submission class)
#SBATCH --partition tcourt

# Job time (hh:mm:ss)
#SBATCH --time 00:20:00

# ----------------------------
# Compute nodes number
#SBATCH --nodes 1

# MPI tasks per compute node
#SBATCH --ntasks-per-node 1

#SBATCH --cpus-per-task 1

# Memory per compute node (MB)
#SBATCH --mem 120000 
# ----------------------------

#SBATCH --mail-type ALL
# User e-mail address
####SBATCH --mail-user firstname.name@address.fr

# Compiler / MPI environments
# ------------------------------
module purge >& /dev/null
module load compilers/intel/2017
module load mpi/intelmpi/2017
# ------------------------------

EXE=TV_joint_V2

LOG=mpicode.${SLURM_NTASKS}
INPFILES="INPUTS/Template01_img2_larger.bin INPUTS/Reference01_img1_larger.bin INPUTS/edge_detector.bin"

cp $EXE $INPFILES $LOCAL_WORK_DIR

cd $LOCAL_WORK_DIR
echo Working directory : $PWD

# MPI profiling
export I_MPI_STATS=ipm
export I_MPI_STATS_FILE=mpistats.out.${SLURM_NTASKS}

set -x

# MPI code execution
srun ./$EXE | tee $LOG 

set +x

# Copy back output files
# ------------------------------
OUTDIR=$SLURM_SUBMIT_DIR/BROADWELL_OUTPUTS/${SLURM_JOB_NAME}/${SLURM_JOB_ID}_${SLURM_NTASKS}tasks_${OMP_NUM_THREADS}threads
mkdir -p $OUTDIR

mv $EXE $I_MPI_STATS_FILE $LOG final_interpolant_V1.bin $OUTDIR
if test -f gmon.out; then mv gmon.out $OUTDIR; fi
# ------------------------------

