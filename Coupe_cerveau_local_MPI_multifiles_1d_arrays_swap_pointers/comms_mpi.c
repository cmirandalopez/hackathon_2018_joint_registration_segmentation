#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "mpi_params.h"
#include "functions.h"
#include "dimens.h"

MPI_Datatype type_ligne, type_colonne_mono;

// ----
 void cree_type(int * tab_bounds) {

  MPI_Type_contiguous(tab_bounds[3]-tab_bounds[2]+1,MPI_DOUBLE,&type_ligne);
  MPI_Type_commit(&type_ligne);
  
  MPI_Type_vector(tab_bounds[1]-tab_bounds[0]+1,1,tab_bounds[3]-tab_bounds[2]+3, MPI_DOUBLE, &type_colonne_mono);
  MPI_Type_commit(&type_colonne_mono); 

 }

// ----
 void communication_V(double * V11_local_mat, double * V12_local_mat,double * V21_local_mat, double * V22_local_mat, int * tab_bounds,MPI_Comm comm2d,int * voisin) {
 
  const int etiquette = 800;
  MPI_Status statut;
  int sx,ex,sy,ey;
  sx=1;
  sy=1;
  ex=tab_bounds[1]-tab_bounds[0]+1;
  ey=tab_bounds[3]-tab_bounds[2]+1;
 
  
  /* Envoi au voisin N et reception du voisin S */
  

  MPI_Sendrecv(&V11_local_mat(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &V11_local_mat(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  MPI_Sendrecv(&V12_local_mat(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &V12_local_mat(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut);

  MPI_Sendrecv(&V21_local_mat(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &V21_local_mat(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  MPI_Sendrecv(&V22_local_mat(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &V22_local_mat(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut);

  
  /* Envoi au voisin S et reception du voisin N */

  
  MPI_Sendrecv(&(V11_local_mat(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(V11_local_mat(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);

  MPI_Sendrecv(&(V12_local_mat(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(V12_local_mat(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
  MPI_Sendrecv(&(V21_local_mat(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(V21_local_mat(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);

  MPI_Sendrecv(&(V22_local_mat(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(V22_local_mat(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  

 
  /* Envoi au voisin W et  reception du voisin E */
 
  MPI_Sendrecv(&(V11_local_mat(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(V11_local_mat(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(V12_local_mat(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(V12_local_mat(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(V21_local_mat(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(V21_local_mat(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(V22_local_mat(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(V22_local_mat(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  

  /* Envoi au voisin E et  reception du voisin W */


  MPI_Sendrecv(&(V11_local_mat(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(V11_local_mat(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  MPI_Sendrecv(&(V12_local_mat(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(V12_local_mat(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  MPI_Sendrecv(&(V21_local_mat(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(V21_local_mat(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(V22_local_mat(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(V22_local_mat(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
 
  

}

// ----
  void communication(double *U1_local_mat, double *U2_local_mat, int * tab_bounds ,MPI_Comm comm2d, int * voisin, int * voisin_diagonale) {

  const int etiquette = 100;
  MPI_Status statut;
  int sx,ex,sy,ey;
  sx=1;
  sy=1;
  ex=tab_bounds[1]-tab_bounds[0]+1;
  ey=tab_bounds[3]-tab_bounds[2]+1;
 
  
  /* Envoi au voisin N et reception du voisin S */
  
  MPI_Sendrecv(&U1_local_mat(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &U1_local_mat(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  MPI_Sendrecv(&U2_local_mat(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &U2_local_mat(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut);

  
  
  /* Envoi au voisin S et reception du voisin N */
  MPI_Sendrecv(&(U1_local_mat(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(U1_local_mat(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
  MPI_Sendrecv(&(U2_local_mat(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(U2_local_mat(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
 
  

 
  /* Envoi au voisin W et  reception du voisin E */
 
  MPI_Sendrecv(&(U1_local_mat(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(U1_local_mat(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(U2_local_mat(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
 

  /* Envoi au voisin E et  reception du voisin W */
 
  
  MPI_Sendrecv(&(U1_local_mat(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(U1_local_mat(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(U2_local_mat(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  

  /*Envoi au voisin NE et reception du voisin SO*/

  
  MPI_Sendrecv(&(U1_local_mat(sx, ey)),1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		&(U1_local_mat(ex+1, sy-1)), 1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat(sx, ey)),1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		&(U2_local_mat(ex+1, sy-1)), 1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		comm2d, &statut);
  

  /*Envoi au voisin SO et reception du voisin NE*/


  MPI_Sendrecv(&(U1_local_mat(ex, sy)),1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		&(U1_local_mat(sx-1, ey+1)), 1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		comm2d, &statut);

  MPI_Sendrecv(&(U2_local_mat(ex, sy)),1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		&(U2_local_mat(sx-1, ey+1)), 1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		comm2d, &statut);


  /*Envoi au voisin ES et reception du voisin ON*/

  
  MPI_Sendrecv(&(U1_local_mat(ex, ey)),1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		&(U1_local_mat(sx-1, sy-1)), 1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat(ex, ey)),1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		&(U2_local_mat(sx-1, sy-1)), 1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		comm2d, &statut);
 
 

  /*Envoi au voisin ON et reception du voisin ES*/

  MPI_Sendrecv(&(U1_local_mat(sx, sy)),1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		&(U1_local_mat(ex+1, ey+1)), 1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat(sx, sy)),1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		&(U2_local_mat(ex+1, ey+1)), 1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		comm2d, &statut);
 


  /*Traitement des cas particuliers pour les envois diagonaux*/

  
  
  if(voisin[North]==MPI_PROC_NULL){  
  
  MPI_Sendrecv(&(U1_local_mat(sx-1, ey)),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(U1_local_mat(sx-1, sy-1)), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);

  
  MPI_Sendrecv(&(U2_local_mat(sx-1, ey)),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(U2_local_mat(sx-1, sy-1)), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);

 
  }

  if(voisin[North]==MPI_PROC_NULL){

 
  MPI_Sendrecv(&(U1_local_mat(sx-1, sy)),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(U1_local_mat(sx-1, ey+1)), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);
  
  MPI_Sendrecv(&(U2_local_mat(sx-1, sy)),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(U2_local_mat(sx-1, ey+1)), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);

  }


 

  if(voisin[W]==MPI_PROC_NULL){
   

  
  MPI_Sendrecv(&(U1_local_mat(ex, sy-1)),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(U1_local_mat(sx-1, sy-1)), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);
 MPI_Sendrecv(&(U2_local_mat(ex, sy-1)),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(U2_local_mat(sx-1, sy-1)), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);
  

  }

  
  if(voisin[W]==MPI_PROC_NULL){

 
  MPI_Sendrecv(&(U1_local_mat(sx, sy-1)),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(U1_local_mat(ex+1, sy-1)), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);
  
MPI_Sendrecv(&(U2_local_mat(sx, sy-1)),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(U2_local_mat(ex+1, sy-1)), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);
  
  }

  
   /**********************************************!!!!!!*****************/
  if(voisin[S]==MPI_PROC_NULL){

 
  MPI_Sendrecv(&(U1_local_mat(ex+1, sy)),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(U1_local_mat(ex+1, ey+1)), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat(ex+1, sy)),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(U2_local_mat(ex+1, ey+1)), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);
  
  }

  
  if(voisin[S]==MPI_PROC_NULL){

  
  MPI_Sendrecv(&(U1_local_mat(ex+1, ey)),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(U1_local_mat(ex+1, sy-1)), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);
 MPI_Sendrecv(&(U2_local_mat(ex+1, ey)),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(U2_local_mat(ex+1, sy-1)), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);
  


  }

 if(voisin[E]==MPI_PROC_NULL){

  
  MPI_Sendrecv(&(U1_local_mat(ex, ey+1)),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(U1_local_mat(sx-1, ey+1)), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(U2_local_mat(ex, ey+1)),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(U2_local_mat(sx-1, ey+1)), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);

  }

  if(voisin[E]==MPI_PROC_NULL){

  
  MPI_Sendrecv(&(U1_local_mat(sx, ey+1)),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(U1_local_mat(ex+1, ey+1)), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);
 
MPI_Sendrecv(&(U2_local_mat(sx, ey+1)),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(U2_local_mat(ex+1, ey+1)), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);

  }
 
  

}

// ----
 void communication_interpolation(double **interpolant_local,double **dx_interpolant_local,double **dy_interpolant_local,int * tab_bounds,MPI_Comm comm2d,int * voisin) {

  const int etiquette = 500;
  MPI_Status statut;
  int sx,ex,sy,ey;
  sx=1;
  sy=1;
  ex=tab_bounds[1]-tab_bounds[0]+1;
  ey=tab_bounds[3]-tab_bounds[2]+1;
 
  
  /* Envoi au voisin N et reception du voisin S */
  
  MPI_Sendrecv(&interpolant_local(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &interpolant_local(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  MPI_Sendrecv(&dx_interpolant_local(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &dx_interpolant_local(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut);

  MPI_Sendrecv(&dy_interpolant_local(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &dy_interpolant_local(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

 

  
  /* Envoi au voisin S et reception du voisin N */
  MPI_Sendrecv(&(interpolant_local(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(interpolant_local(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
  MPI_Sendrecv(&(dx_interpolant_local(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(dx_interpolant_local(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
  MPI_Sendrecv(&(dy_interpolant_local(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(dy_interpolant_local(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);

  

 
  /* Envoi au voisin W et  reception du voisin E */
 
  MPI_Sendrecv(&(interpolant_local(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(interpolant_local(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(dx_interpolant_local(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(dx_interpolant_local(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(dy_interpolant_local(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(dy_interpolant_local(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  
  

  /* Envoi au voisin E et  reception du voisin W */
 
  
  MPI_Sendrecv(&(interpolant_local(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(interpolant_local(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(dx_interpolant_local(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(dx_interpolant_local(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  MPI_Sendrecv(&(dy_interpolant_local(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(dy_interpolant_local(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  
  

}

// ----
  void communication_f (double *f_local, int * tab_bounds, MPI_Comm comm2d, int * voisin) {

  const int etiquette = 900;
  MPI_Status statut;
  int sx,ex,sy,ey;
  sx=1;
  sy=1;
  ex=tab_bounds[1]-tab_bounds[0]+1;
  ey=tab_bounds[3]-tab_bounds[2]+1;
 
  
  /* Envoi au voisin N et reception du voisin S */
  
  MPI_Sendrecv(&f_local(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &f_local(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  
  /* Envoi au voisin S et reception du voisin N */
  MPI_Sendrecv(&(f_local(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(f_local(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  
 
  /* Envoi au voisin W et  reception du voisin E */
 
  MPI_Sendrecv(&(f_local(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(f_local(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  
  

  /* Envoi au voisin E et  reception du voisin W */
 
  
  MPI_Sendrecv(&(f_local(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(f_local(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
 
  

}

// ----
  void communication_divergence(double *p1_local,double *p2_local, int * tab_bounds,MPI_Comm comm2d,int * voisin, int * voisin_diagonale) {

  const int etiquette = 300;
  MPI_Status statut;
  int sx,ex,sy,ey;
  sx=1;
  sy=1;
  ex=tab_bounds[1]-tab_bounds[0]+1;
  ey=tab_bounds[3]-tab_bounds[2]+1;
 
  
  /* Envoi au voisin N et reception du voisin S */
  
  MPI_Sendrecv(&p2_local(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &p2_local(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 
  MPI_Sendrecv(&p1_local(sx, sy), 1, type_ligne, voisin[North], etiquette, 
	       &p1_local(ex+1, sy), 1, type_ligne, voisin[S], etiquette, 
	       comm2d, &statut); 

  /* Envoi au voisin S et reception du voisin N */
  MPI_Sendrecv(&(p2_local(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(p2_local(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);
  MPI_Sendrecv(&(p1_local(ex, sy)), 1, type_ligne, voisin[S], etiquette, 
	       &(p1_local(sx-1, sy)), 1, type_ligne, voisin[North], etiquette, 
	       comm2d, &statut);

  

 
  /* Envoi au voisin W et  reception du voisin E */
 
  MPI_Sendrecv(&(p1_local(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(p1_local(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(p2_local(sx, sy)), 1, type_colonne_mono , voisin[W], etiquette, 
		&(p2_local(sx, ey+1)), 1, type_colonne_mono , voisin[E], etiquette, 
		comm2d, &statut);
  

  /* Envoi au voisin E et  reception du voisin W */
 
  
  MPI_Sendrecv(&(p1_local(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(p1_local(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);
  
  MPI_Sendrecv(&(p2_local(sx, ey)), 1, type_colonne_mono, voisin[E], etiquette, 
		&(p2_local(sx, sy-1)), 1, type_colonne_mono, voisin[W], etiquette, 
		comm2d, &statut);

  /*Envoi au voisin NE et reception du voisin SO*/

  
  MPI_Sendrecv(&(p1_local(sx, ey)),1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		&(p1_local(ex+1, sy-1)), 1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(p2_local(sx, ey)),1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		&(p2_local(ex+1, sy-1)), 1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		comm2d, &statut);
  

  /*Envoi au voisin SO et reception du voisin NE*/


  MPI_Sendrecv(&(p1_local(ex, sy)),1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		&(p1_local(sx-1, ey+1)), 1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		comm2d, &statut);

  MPI_Sendrecv(&(p2_local(ex, sy)),1,MPI_DOUBLE, voisin_diagonale[SO], etiquette, 
		&(p2_local(sx-1, ey+1)), 1,MPI_DOUBLE, voisin_diagonale[NE], etiquette, 
		comm2d, &statut);


  /*Envoi au voisin ES et reception du voisin ON*/

  
  MPI_Sendrecv(&(p1_local(ex, ey)),1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		&(p1_local(sx-1, sy-1)), 1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(p2_local(ex, ey)),1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		&(p2_local(sx-1, sy-1)), 1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		comm2d, &statut);
 
 

  /*Envoi au voisin ON et reception du voisin ES*/

  MPI_Sendrecv(&(p1_local(sx, sy)),1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		&(p1_local(ex+1, ey+1)), 1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(p2_local(sx, sy)),1,MPI_DOUBLE, voisin_diagonale[ON], etiquette, 
		&(p2_local(ex+1, ey+1)), 1,MPI_DOUBLE, voisin_diagonale[ES], etiquette, 
		comm2d, &statut);
 


  /*Traitement des cas particuliers pour les envois diagonaux*/

  
  
  if(voisin[North]==MPI_PROC_NULL){  
  
  MPI_Sendrecv(&(p1_local(sx-1, ey)),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(p1_local(sx-1, sy-1)), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);

  
  MPI_Sendrecv(&(p2_local(sx-1, ey)),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(p2_local(sx-1, sy-1)), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);

 
  }

  if(voisin[North]==MPI_PROC_NULL){

 
  MPI_Sendrecv(&(p1_local(sx-1, sy)),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(p1_local(sx-1, ey+1)), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);
  
  MPI_Sendrecv(&(p2_local(sx-1, sy)),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(p2_local(sx-1, ey+1)), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);

  }


 

  if(voisin[W]==MPI_PROC_NULL){
   

  
  MPI_Sendrecv(&(p1_local(ex, sy-1)),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(p1_local(sx-1, sy-1)), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);
 MPI_Sendrecv(&(p2_local(ex, sy-1)),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(p2_local(sx-1, sy-1)), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);
  

  }

  
  if(voisin[W]==MPI_PROC_NULL){

 
  MPI_Sendrecv(&(p1_local(sx, sy-1)),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(p1_local(ex+1, sy-1)), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);
  
MPI_Sendrecv(&(p2_local(sx, sy-1)),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(p2_local(ex+1, sy-1)), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);
  
  }

  
   /**********************************************!!!!!!*****************/
  if(voisin[S]==MPI_PROC_NULL){

 
  MPI_Sendrecv(&(p1_local(ex+1, sy)),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(p1_local(ex+1, ey+1)), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(p2_local(ex+1, sy)),1,MPI_DOUBLE, voisin[W], etiquette, 
		&(p2_local(ex+1, ey+1)), 1,MPI_DOUBLE, voisin[E], etiquette, 
		comm2d, &statut);
  
  }

  
  if(voisin[S]==MPI_PROC_NULL){

  
  MPI_Sendrecv(&(p1_local(ex+1, ey)),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(p1_local(ex+1, sy-1)), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);
 MPI_Sendrecv(&(p2_local(ex+1, ey)),1,MPI_DOUBLE, voisin[E], etiquette, 
		&(p2_local(ex+1, sy-1)), 1,MPI_DOUBLE, voisin[W], etiquette, 
		comm2d, &statut);
  


  }

 if(voisin[E]==MPI_PROC_NULL){

  
  MPI_Sendrecv(&(p1_local(ex, ey+1)),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(p1_local(sx-1, ey+1)), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);
  MPI_Sendrecv(&(p2_local(ex, ey+1)),1,MPI_DOUBLE, voisin[S], etiquette, 
		&(p2_local(sx-1, ey+1)), 1,MPI_DOUBLE, voisin[North], etiquette, 
		comm2d, &statut);

  }

  if(voisin[E]==MPI_PROC_NULL){

  
  MPI_Sendrecv(&(p1_local(sx, ey+1)),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(p1_local(ex+1, ey+1)), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);
 
MPI_Sendrecv(&(p2_local(sx, ey+1)),1,MPI_DOUBLE, voisin[North], etiquette, 
		&(p2_local(ex+1, ey+1)), 1,MPI_DOUBLE, voisin[S], etiquette, 
		comm2d, &statut);

  }
  
  

}
