#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <mpi.h>

#ifdef MKL_BLAS
#include <mkl_cblas.h>
#include <mkl_lapack.h>
#else
#include <cblas.h>
#endif

#include "params.h"
#include "functions.h"
#include "dimens.h"

void resolution(double t1, double* restrict R_local_mat, double* restrict g_local_mat,int M_R, int N_R, double* restrict U1_local_mat, double* restrict U2_local_mat, double* restrict V11_local_mat, double* restrict V12_local_mat,
                double* restrict V21_local_mat, double* restrict V22_local_mat, double* restrict U1_local_mat_new, double* restrict U2_local_mat_new, double* restrict V11_local_mat_new, double* restrict V12_local_mat_new,
                double* restrict V21_local_mat_new, double* restrict V22_local_mat_new, int * restrict tab_bounds, int * restrict voisin, int * restrict voisin_diagonale, int rang, double theta_lip, int ntx,int nty,
                MPI_Comm comm2d){
		

	int i,j,k,l; 
        


	double c1;	//Moyenne de R a l'interieur du contour de Ttilde pour Chan-Vese
	double c2;
	int nb;
	double c0_U1=1.0+dt*4.0*epsilon2;
	double c1_global;
        double c2_global; 
        int nb_global;
        double comp1;
        double comp2;
        double comp3;
        double comp4;
        double comp5;
        double comp6;
        double den;
        double* tempo; 

	/* *********************************************/
	/* coefficients Ã©lasticitÃ© non linÃ©aire        */
	/* *********************************************/

	double a1= -(coeff_lame_lambda+coeff_lame_mu)/2.0;
	double a2= (coeff_lame_lambda+2.0*coeff_lame_mu)/8.0; //Coefficient beta du papier dans la fonction de densitÃ© quasiconvexifiÃ©e
	double alpha_q=2.0*(coeff_lame_lambda+coeff_lame_mu)/(coeff_lame_lambda+2.0*coeff_lame_mu);//Coefficient alpha du papier dans la fonction de densitÃ© quasiconvexifiÃ©e
	double d;
	double H,dir,coeff2,coeff4,detV_2,c01;
	c01=1.0+dt*epsilon2;
        double gradient2,gradient1;
        double divergence,divergence_plusi,divergence_plusj,norme; 

        const int nbytes = Nelem * sizeof(double);

        double * restrict interpolant_local = malloc (nbytes) ; checkalloc (interpolant_local);
        init_mat1D (interpolant_local, Nelem);   
 
        double * dx_interpolant_local = malloc (nbytes) ; checkalloc (dx_interpolant_local);
        double * dy_interpolant_local = malloc (nbytes) ; checkalloc (dy_interpolant_local);
        init_mat1D (dx_interpolant_local, Nelem); 
        init_mat1D (dy_interpolant_local, Nelem); 

        double * Id1U1_local = malloc (nbytes) ; checkalloc (Id1U1_local);
        double * Id2U2_local = malloc (nbytes) ; checkalloc (Id2U2_local);
 
       
        double ** C=allocarray(M_R,N_R);
  
        double * restrict T_tilde_local = malloc (nbytes) ; checkalloc (T_tilde_local);
        
        int ** Id1=allocarray_int(M_R,N_R); /*direction des colonnes/direction des x*/ //-->Global
        int ** Id2=allocarray_int(M_R,N_R); /*direction des lignes/direction des y*/ //-->Global
        if(rang==0){
        for(i=0;i<M_R;i++){
		for(j=0;j<N_R;j++){
			Id1[i][j]=j+1;
			Id2[i][j]=i+1;
		}
	}
        }

        MPI_Bcast(&(Id1[0][0]),M_R*N_R,MPI_INT,0,comm2d); 
        MPI_Bcast(&(Id2[0][0]),M_R*N_R,MPI_INT,0,comm2d);

      
        /*Variable auxiliaire pour la projection de Chambolle*/
        double * restrict f_local = malloc (nbytes) ; checkalloc (f_local);
        init_mat1D (f_local, Nelem);
        
        /*Variables pour l'algorithme de projection de Chambolle*/
       
        double * p1_local = malloc (nbytes) ; checkalloc (p1_local);
        init_mat1D (p1_local, Nelem);
         
        double * p2_local = malloc (nbytes) ; checkalloc (p2_local);
        init_mat1D (p2_local, Nelem);

        double * p1_local_new = malloc (nbytes) ; checkalloc (p1_local_new);
        double * p2_local_new = malloc (nbytes) ; checkalloc (p2_local_new);
        
  
        
        
        /************************************************************************************************/
        /*Pour l'instant, cette tâche n'est effectuée que par le rang 0 qui distribue ensuite les éléments*/
        /**************************************************************************************************/
        /*Déclaration des éléments dont on a besoin*/
        
        FILE *data_T;
        double * T_global_vect;
        double ** T_global_mat;
        

        if(rang==0){
        
        T_global_vect=malloc(M_R*N_R*sizeof(double));
        T_global_mat=allocarray(M_R,N_R);
        data_T = fopen("Template01_img2_larger.bin","r");
        fread(T_global_vect,sizeof(T_global_vect),N_R*M_R,data_T);
        fclose(data_T);
        	for (i=0;i<M_R;i++) {
	        T_global_mat[i] = &(T_global_vect[i*N_R]);
                }

        coeff_spline_multiscale(M_R,N_R,T_global_mat,C,theta_lip);


        }
        /*Diffusion de C à l'ensemble des processeurs*/

        MPI_Bcast(&(C[0][0]),M_R*N_R,MPI_DOUBLE,0,comm2d);
         
        /*****************Interpolation effectuée localement par les processeurs********/ 
        /*Création de Id1U1_local*/
        /*Création de Id2U2_local*/
        for(int i=0;i<(tab_bounds[1]-tab_bounds[0]+3);i++){

                     for(int j=0;j<(tab_bounds[3]-tab_bounds[2]+3);j++){
 
                     Id1U1_local(i ,j)=1.0*Id1[tab_bounds[0]-1+i][tab_bounds[2]-1+j] + U1_local_mat(i, j);
                     Id2U2_local(i, j)=1.0*Id2[tab_bounds[0]-1+i][tab_bounds[2]-1+j] + U2_local_mat(i, j);
                     }


        }

        /*Processus d'interpolation réalisé localement par chacun des processeurs*/ 
        
        interpol2D( tab_bounds, Nelem_total_x, Nelem_total_y, Id2U2_local, Id1U1_local, C, interpolant_local, dx_interpolant_local,
                    dy_interpolant_local, M_R, N_R ); 

        copie_mat1D (T_tilde_local, interpolant_local, Nelem);
      
        /* Creation types derives MPI */
        cree_type (tab_bounds);

        double t_init = MPI_Wtime();

        if (rang == 0) {
            printf ("Duree initialisation %f secs\n", t_init - t1);
        }
 
        /* ************************** */
	/* **** BOUCLE RESOLUTION ****/
	/* ************************* */
#pragma omp parallel private(k)
{
        for(k=0;k<IterMax;k++){
			
			if(rang==0){
				#pragma omp single
				{
					printf("iteration par rapport au temps : %d \n",k);
                        	}
			}
                        
			c1=0.0;
                        c1_global=0.0;
			c2=0.0;
                        c2_global=0.0;
			nb=0;
                        nb_global=0; 

                        calcul_V (tab_bounds, V11_local_mat_new, V22_local_mat_new, V12_local_mat_new, V21_local_mat_new, U1_local_mat, U2_local_mat, T_tilde_local, 
                                V11_local_mat, V22_local_mat, V12_local_mat, V21_local_mat,
                                R_local_mat, coeff_lame_mu, alpha_q, c01, dt, a2, rho, epsilon2, eps);
			
			#pragma omp for reduction(+: c1,c2,nb) 
                        for (int i=1; i < (tab_bounds[1]-tab_bounds[0]+2); i++) {
#pragma omp simd reduction(+:c1,nb,c2)
                             for (int j=1; j < (tab_bounds[3]-tab_bounds[2]+2); j++) {

                                  if (T_tilde_local(i, j)>rho){
                                      c1 = c1 + R_local_mat(i, j);
                                      nb = nb + 1;
                                  }
                                  else c2 = c2 + R_local_mat(i, j);
                             
                             }
                        }
								
                        /*copie_mat1D(V11_local_mat, V11_local_mat_new, Nelem);
                        copie_mat1D(V12_local_mat, V12_local_mat_new, Nelem);
                        copie_mat1D(V21_local_mat, V21_local_mat_new, Nelem);
                        copie_mat1D(V22_local_mat, V22_local_mat_new, Nelem);*/
			#pragma omp single 
                        {
			/*	swap_pointers(&V11_local_mat, &V11_local_mat_new);
				swap_pointers(&V12_local_mat, &V12_local_mat_new);
				swap_pointers(&V21_local_mat, &V21_local_mat_new);
				swap_pointers(&V22_local_mat, &V22_local_mat_new);
			*/
				tempo=V11_local_mat;	V11_local_mat=V11_local_mat_new; 	V11_local_mat_new=tempo;
				tempo=V12_local_mat;	V12_local_mat=V12_local_mat_new; 	V12_local_mat_new=tempo;
				tempo=V21_local_mat;	V21_local_mat=V21_local_mat_new; 	V21_local_mat_new=tempo;
				tempo=V22_local_mat;	V22_local_mat=V22_local_mat_new; 	V22_local_mat_new=tempo;
				
				communication_V(V11_local_mat,V12_local_mat,V21_local_mat,V22_local_mat,tab_bounds,comm2d,voisin);
                     
                        	MPI_Allreduce(&c1,&c1_global,1,MPI_DOUBLE,MPI_SUM,comm2d);
                        	MPI_Allreduce(&c2,&c2_global,1,MPI_DOUBLE,MPI_SUM,comm2d);
                        	MPI_Allreduce(&nb,&nb_global,1,MPI_INT,MPI_SUM,comm2d);
                        	
				c1_global=c1_global/(double)nb_global;
				c2_global=c2_global/(double)((M_R-2)*(N_R-2)-nb_global);
                        } 
			
			// OMP dans calcul_U
                        calcul_U (tab_bounds, voisin, voisin_diagonale, comm2d, n_max, theta, lambdanl, U1_local_mat_new, U2_local_mat_new, U1_local_mat, U2_local_mat, R_local_mat,                                  
                                  V11_local_mat, V12_local_mat, V21_local_mat, V22_local_mat, dx_interpolant_local, dy_interpolant_local, interpolant_local, f_local, T_tilde_local, dt, epsilon2, c0_U1);

        /*Actualisation de l'interpolant*/
        #pragma omp for 
        	for(int i=0;i<(tab_bounds[1]-tab_bounds[0]+3);i++){

                	     for(int j=0;j<(tab_bounds[3]-tab_bounds[2]+3);j++){
 
                     		Id1U1_local(i, j) = 1.0 * Id1[tab_bounds[0]-1+i][tab_bounds[2]-1+j] + U1_local_mat(i, j);
                     		Id2U2_local(i, j) = 1.0 * Id2[tab_bounds[0]-1+i][tab_bounds[2]-1+j] + U2_local_mat(i, j);
                     	} 


        	}

        /*Processus d'interpolation réalisé localement par chacun des processeurs*/ 
     	// OMP dans interpol2D   
        interpol2D ( tab_bounds, Nelem_total_x, Nelem_total_y, Id2U2_local, Id1U1_local, C, interpolant_local, dx_interpolant_local, 
                      dy_interpolant_local, M_R, N_R);
                         


                          /****************************************FIN DE LA PHASE d'INTERPOLATION AVEC LE NOUVEAU CHAMP DE DEPLACEMENT*********************/
for( int l=0;l<n_max;l++) {
		#pragma omp for private(divergence, divergence_plusi, divergence_plusj, comp1, comp2, comp3, comp4, comp5, comp6, gradient1, gradient2, norme, den)     
		for( int i=1; i<(tab_bounds[1]-tab_bounds[0]+2);i++) { // Parce qu'il y a les cellules fantÃ´mes
#pragma omp simd private(gradient1, gradient2)
				for(int j=1;j<(tab_bounds[3]-tab_bounds[2]+2);j++) { // Parce qu'il y a les cellules fantÃ´mes


                                                                 divergence = p1_local(i, j)-p1_local(i, j-1)+p2_local(i, j)-p2_local(i-1, j);
                                                                 divergence_plusi=p1_local(i+1, j)-p1_local(i+1, j-1)+p2_local(i+1, j)-p2_local(i, j);
                                                                 divergence_plusj=p1_local(i, j+1)-p1_local(i, j)+p2_local(i, j+1)-p2_local(i-1, j+1);
                                                                

                                                          

                                                                 comp1=interpolant_local(i+1, j)-f_local(i+1, j)-theta*lambdal2*(c1_global-R_local_mat(i+1, j))*(c1_global-R_local_mat(i+1, j));     
                                                                 comp2=theta*lambdal2*(c2_global-R_local_mat(i+1, j))*(c2_global-R_local_mat(i+1, j));
                                                                 comp3=interpolant_local(i, j)-f_local(i, j)-theta*lambdal2*(c1_global-R_local_mat(i, j))*(c1_global-R_local_mat(i, j));
                                                                 comp4=theta*lambdal2*(c2_global-R_local_mat(i, j))*(c2_global-R_local_mat(i, j)); 
                                                                 gradient2=divergence_plusi-(comp1+comp2)/theta-divergence+(comp3+comp4)/theta;




								 comp5=interpolant_local(i, j+1)-f_local(i, j+1)-theta*lambdal2*(c1_global-R_local_mat(i, j+1))*(c1_global-R_local_mat(i, j+1));
                                                                 comp6=theta*lambdal2*(c2_global-R_local_mat(i, j+1))*(c2_global-R_local_mat(i, j+1));            									           		   								
								 gradient1=divergence_plusj-(comp5+comp6)/theta-divergence+(comp3+comp4)/theta;
							





							norme=SQRT(gradient1*gradient1+gradient2*gradient2);
							//norme=sqrt(gradient1*gradient1+gradient2*gradient2);

                                                        den=1.0+(tau*norme)/g_local_mat(i, j);
							p1_local_new(i, j)=(p1_local(i, j)+tau*gradient1)/den;

							p2_local_new(i, j)=(p2_local(i, j)+tau*gradient2)/den;
				}
		}

                #pragma omp for                      
                for(i=1;i<(tab_bounds[1]-tab_bounds[0]+2);i++){// Parce qu'il y a les cellules fantÃ´mes
			for(j=1;j<(tab_bounds[3]-tab_bounds[2]+2);j++){// Parce qu'il y a les cellules fantÃ´mes
				p1_local(i, j)=p1_local_new(i, j);
				p2_local(i, j)=p2_local_new(i, j);
			}
		}
		               
		#pragma omp single
		{                   
		communication_divergence(p1_local,p2_local,tab_bounds,comm2d,voisin,voisin_diagonale);
 		}
	}// Fin de l'actualisation du champ de vecteurs p
         
      	
	#pragma omp for private(divergence)
        for(i=1;i<(tab_bounds[1]-tab_bounds[0]+2);i++){// Parce qu'il y a les cellules fantÃ´mes
		for(j=1;j<(tab_bounds[3]-tab_bounds[2]+2);j++){// Parce qu'il y a les cellules fantÃ´mes
				
                	divergence=p1_local(i, j)-p1_local(i, j-1)+p2_local(i, j)-p2_local(i-1, j);

			T_tilde_local(i, j)=(interpolant_local(i, j)-f_local(i, j))-divergence*theta-theta*lambdal2*(c1_global-R_local_mat(i, j))*(c1_global-R_local_mat(i, j))+theta*lambdal2*(c2_global-R_local_mat(i, j))*(c2_global-R_local_mat(i, j));

			if(interpolant_local(i, j)-T_tilde_local(i, j)>=theta*epsilon1)
				f_local(i, j)=interpolant_local(i, j)-T_tilde_local(i, j)-theta*epsilon1;
			else if(interpolant_local(i, j)-T_tilde_local(i, j)<=-theta*epsilon1)
				f_local(i, j)=interpolant_local(i, j)-T_tilde_local(i, j)+theta*epsilon1;
			else
				f_local(i, j)=0.0;
		}
	}
 /*********************************************ALGORITHME DE PROJECTION DE CHAMBOLLE***********************************************/
        #pragma omp single
	{
		communication_f(f_local,tab_bounds,comm2d,voisin);    
        }              		
       
       

    }//fin de la boucle k
} // fin pragma
    double t_resol = MPI_Wtime();

    if (rang == 0) {
        printf ("Temps total en %f secs (boucle en temps uniquement)\n", t_resol - t_init);

    } 

    ecrire_mpi (interpolant_local, ntx, nty, tab_bounds, comm2d, "final_interpolant_V1.bin"); 
       
printf("MIRACLE\n");        
}

// ----

#define M_PI 3.14159265359

// calcul la fonction regularisee de Heaviside 
 double heaviside(double phi,double eps){

         return(0.5*(1.0+(2.0/M_PI)*atan(phi/eps)));

 }

// ----
//dirac regularisee
 double dirac(double phi,double eps){

        return((eps/M_PI)/(eps*eps+phi*phi));
 }

// ---
#pragma omp declare simd
 double SQRT(double number) {
    long i;
    double x, y;
    const double f = 1.5F;

    x = number * 0.5F;
    y  = number;
    i  = * ( long * ) &y;
    i  = 0x5f3759df - ( i >> 1 );
    y  = * ( double * ) &i;
    y  = y * ( f - ( x * y * y ) );
    y  = y * ( f - ( x * y * y ) );
    return number * y;
 }

void coeff_spline_multiscale(int M, int N,double ** T,double ** C, double theta_lip){


	double ** Bm;
	double ** Bn;
	double ** IN;
	double ** IM;
	double ** Dn;
	double ** Dm;


	

	int i,j;


	Bm=(double**)calloc(N,sizeof(double *));
	Dm=(double**)calloc(N-1,sizeof(double *));
	IN=(double**)calloc(N,sizeof(double *));

	Bn=(double**)calloc(M,sizeof(double *));
	Dn=(double**)calloc(M-1,sizeof(double *));
	IM=(double**)calloc(M,sizeof(double *));



	for(i=0;i<N;i++){

		Bm[i]=(double*)calloc(N,sizeof(double));
		IN[i]=(double*)calloc(N,sizeof(double));
	}
	for(i=0;i<N-1;i++){
		Dm[i]=(double*)calloc(N,sizeof(double));
	}

	for(i=0;i<M;i++){

		Bn[i]=(double*)calloc(M,sizeof(double));
		IM[i]=(double*)calloc(M,sizeof(double));
	}
	for(i=0;i<M-1;i++){
		Dn[i]=(double*)calloc(M,sizeof(double));
	}

	Bm[0][0]=4.0;
	Bm[0][1]=1.0;
	IN[0][0]=1.0;
	IM[0][0]=1.0;

	for(i=1;i<N-1;i++){
		Bm[i][i]=4.0;
		Bm[i][i-1]=1.0;
		Bm[i][i+1]=1.0;
		IN[i][i]=1.0;

	}

	Bm[N-1][N-1]=4.0;
	Bm[N-1][N-2]=1.0;
	IN[N-1][N-1]=1.0;
	Bn[0][0]=4.0;
	Bn[0][1]=1.0;
	for(i=1;i<M-1;i++){

		Bn[i][i]=4.0;
		Bn[i][i-1]=1.0;
		Bn[i][i+1]=1.0;
		IM[i][i]=1.0;

	}
	Bn[M-1][M-1]=4.0;
	Bn[M-1][M-2]=1.0;
	IM[M-1][M-1]=1.0;

	for(i=0;i<N-1;i++){
		Dm[i][i]=-1.0;
		Dm[i][i+1]=1.0;
	}
	for(i=0;i<M-1;i++){
		Dn[i][i]=-1.0;
		Dn[i][i+1]=1.0;
	}


	double * Bm_vec;
	double * Dm_vec;
	double * Wm_vec;
	double * Bn_vec;
	double * Dn_vec;
	double * Wn_vec;
	double * IN_vec;
	double * IM_vec;

	Bm_vec=(double*)calloc(N*N,sizeof(double));
	Dm_vec=(double*)calloc(N*(N-1),sizeof(double));
	Wm_vec=(double*)calloc(N*N,sizeof(double));
	IN_vec=(double*)calloc(N*N,sizeof(double));
	Bn_vec=(double*)calloc(M*M,sizeof(double));
	Dn_vec=(double*)calloc(M*(M-1),sizeof(double));
	Wn_vec=(double*)calloc(M*M,sizeof(double));
	IM_vec=(double*)calloc(M*M,sizeof(double));



	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			Bm_vec[i*N+j]=Bm[i][j];
			IN_vec[i*N+j]=IN[i][j];
		}
	}

	for(i=0;i<M;i++){
		for(j=0;j<M;j++){
			Bn_vec[i*M+j]=Bn[i][j];
			IM_vec[i*M+j]=IM[i][j];
		}
	}

	for(i=0;i<N-1;i++){
		for(j=0;j<N;j++){
			Dm_vec[i*N+j]=Dm[i][j];
		}
	}


	for(i=0;i<M-1;i++){
		for(j=0;j<M;j++){
			Dn_vec[i*M+j]=Dn[i][j];
		}
	}

	double alpha;
	double beta;
	beta=0.0;
	alpha=theta_lip;


	cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,N,N,N-1,alpha,IN_vec,N,IN_vec,N,beta,Wm_vec,N);
	cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,M,M,M-1,alpha,IM_vec,M,IM_vec,M,beta,Wn_vec,M);
	/* contient la matrice In*/
	/*contient la matrice Im */

	beta=1.0;
	alpha=1.0;

	cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,N,N,N,alpha,Bm_vec,N,Bm_vec,N,beta,Wm_vec,N);
	cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,M,M,M,alpha,Bn_vec,M,Bn_vec,M,beta,Wn_vec,M);
	/* Wn_vec contient la matrice Mn*/
	/*Wm_vec contient la matrice Mm*/



	int info,lwork;
	double wkopt;
	double * work;
	int dim=N;
	int ipiv[dim];
	int nrhs=N;
	lwork=-1;
	dsysv_("lower",&dim,&nrhs,Wm_vec,&dim,ipiv,IN_vec,&dim,&wkopt,&lwork,&info);
	lwork=(int)wkopt;
	work=(double*)malloc(lwork*sizeof(double));
	dsysv_("lower",&dim,&nrhs,Wm_vec,&dim,ipiv,IN_vec,&dim,work,&lwork,&info);

	/* IN_vec contient la matrice inverse de M_m=Bm^T*Bm+theta*Dn^T*Dn*/

	double * T_vec;
	T_vec=(double*)calloc(M*N,sizeof(double));

	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			T_vec[i*N+j]=T[i][j];
		}
	}

	beta=0.0;
	alpha=1.0;
	double * Bn_dataT_vec;
	Bn_dataT_vec=(double*)calloc(M*N,sizeof(double));
	cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,M,N,M,alpha,Bn_vec,M,T_vec,N,beta,Bn_dataT_vec,N);


	double * Bn_dataT_Bm_vec;
	Bn_dataT_Bm_vec=(double*)calloc(M*N,sizeof(double));

	cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,M,N,N,alpha,Bn_dataT_vec,N,Bm_vec,N,beta,Bn_dataT_Bm_vec,N);

	double * Bn_dataT_Bm_invMm_vec;
	Bn_dataT_Bm_invMm_vec=(double*)calloc(M*N,sizeof(double));

	cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,M,N,N,alpha,Bn_dataT_Bm_vec,N,IN_vec,N,beta,Bn_dataT_Bm_invMm_vec,N);

	/* Bn_dataT_Bm_invMm_vec contient la matrice Bn*data_T*Bm*Mm^(-1) */


	int info1,lwork1;
	double wkopt1;
	double * work1;
	int dim1=M;
	int ipiv1[dim1];
	int nrhs1=M;
	lwork1=-1;

	dsysv_("lower",&dim1,&nrhs1,Wn_vec,&dim1,ipiv1,IM_vec,&dim1,&wkopt1,&lwork1,&info1);
	lwork1=(int)wkopt1;
	work1=(double*)malloc(lwork1*sizeof(double));
	dsysv_("lower",&dim1,&nrhs1,Wn_vec,&dim1,ipiv1,IM_vec,&dim1,work1,&lwork1,&info1);

	/*IM_vec contient la matrice inverse de Mn*/

	beta=0.0;
	alpha=1.0;
	double * C_vec;
	C_vec=(double*)calloc(M*N,sizeof(double));
	cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,M,N,M,alpha,IM_vec,M,Bn_dataT_Bm_invMm_vec,N,beta,C_vec,N);




	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			C[i][j]=C_vec[i*N+j];
		}
	}

	


	for(i=0;i<N;i++){
		free(Bm[i]);
		free(IN[i]);
	}
	for(i=0;i<N-1;i++){
		free(Dm[i]);
	}
	for(i=0;i<M-1;i++){
		free(Dn[i]);
	}
	for(i=0;i<M;i++){
		free(Bn[i]);
		free(IM[i]);
	}
	free(Bm),free(IN),free(Bn),free(IM),free(Dm),free(Dn);
	free(Bm_vec),free(Bn_vec),free(IN_vec),free(IM_vec),free(work),free(work1),free(T_vec),free(Bn_dataT_vec);
	free(Dm_vec),free(Wm_vec),free(Dn_vec),free(Wn_vec),free(Bn_dataT_Bm_vec),free(Bn_dataT_Bm_invMm_vec),free(C_vec);
}

/* --> checked   */
