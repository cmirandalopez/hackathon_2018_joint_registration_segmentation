#include <stdlib.h>
#include <stdio.h>

// ----
   double **allocarray(int Nlig,int Mcol) {
    double **array2 = malloc( Nlig* sizeof( double * ) );
    int i;

    if( array2 != NULL ){
        array2[0] = malloc(Nlig * Mcol * sizeof( double ) );
        if( array2[ 0 ] != NULL ) {
            for( i = 1; i < Nlig; i++ )
                array2[i] = array2[0] + i * Mcol;
        }

        else {
            free(array2);
            array2 = NULL;
            printf("Erreur dans l'allocation mÃ©moire -- phase 2\n");
        }
    }
    return array2;
    }

// ----
   int **allocarray_int(int Nlig,int Mcol) {
    int **array2 = malloc( Nlig* sizeof( int * ) );
    int i;

    if( array2 != NULL ){
        array2[0] = malloc(Nlig * Mcol * sizeof( int ) );
        if( array2[ 0 ] != NULL ) {
            for( i = 1; i < Nlig; i++ )
                array2[i] = array2[0] + i * Mcol;
        }

        else {
            free(array2);
            array2 = NULL;
            printf("Erreur dans l'allocation mÃ©moire -- phase 2\n");
        }
    }
    return array2;
    }

// ----
void init_mat(double ** tab,int Mlig,int Ncol){

#define tab(i, j) tab[i][j]

        int i,j;
        for(i=0;i<Mlig;i++){
                for(j=0;j<Ncol; j++){
                        tab(i, j)=0.0;
                }
        }
}

// ----
void init_mat1D (double * tab, int Ntot) {
         int i;
         for (i = 0; i < Ntot; i++) {
             tab[i] = 0.0;
        }
}                                                                                 


// ----
void copie_mat2D(double** dest, double ** src, int M,int n){

#define dest(i, j) dest[i][j]
#define src(i, j)  src[i][j]

        int i,j;
        for(i=0;i<M;i++){
                for(j=0;j<n;j++){
                        dest(i, j)=src(i, j);
                }
        }
}

// ----
void copie_mat1D(double * dest, double * src, int Ntot) {

        int i;
        for (i = 0; i < Ntot; i++) {
            dest[i] = src[i];
        }
}

void swap_pointers(double * restrict * a, double * restrict * b) {
	double *tmp = *a;
	*a = *b;
	*b = tmp;
}
