#include <mpi.h>

// Calcul
 void resolution (double, double *, double *, int, int, double *, double *, double *, double *, double *, double *, double *, double *, double *, double *, double *, double *, 
                  int *, int *, int *, int, double ,int, int, MPI_Comm);

 void calcul_V (int *, double *, double *, double *, double *, double *, double *, double *,
                double *, double *, double *, double *,
                double *, double, double, double, double, double, double, double, double);

 void calcul_U (int *, int*, int*, MPI_Comm, int, double, double, double *, double *, double *,
                double *, double *, double *, double *, double *, double *,
                double *, double *, double *, double *, double *, double, double, double);

 double heaviside (double, double);
 double dirac (double, double);
 double SQRT (double);
 void coeff_spline_multiscale (int, int, double **, double **, double);

 void interpol2D (int *, int, int, double *, double *, double **, double *, double *, double *, int, int);

// Communications MPI
 void cree_type (int *);
 void communication_V (double *, double *, double *, double *, int *, MPI_Comm, int *);
 void communication (double *, double *, int *, MPI_Comm, int *, int *);
 void communication_interpolation (double **, double **, double **, int *, MPI_Comm, int *);
 void communication_f (double *, int *, MPI_Comm, int *);
 void communication_divergence (double *, double *, int *, MPI_Comm, int *, int *);
 void ecrire_mpi (double *, int, int, int *, MPI_Comm, char *);

// Fonctions utilitaires
 double ** allocarray (int, int);
 int * *allocarray_int (int, int);
 void init_mat (double **, int, int);
 void init_mat1D (double *, int);
 void copie_mat1D (double *, double *, int);
 void copie_mat2D (double **, double **, int, int);
 void swap_pointers(double **,double **);


// Macro pour fonction utilitaire (verification de malloc)
#define communicator comm2d
#define checkalloc(ptr) \
    if (ptr == NULL) { \
          free(ptr); \
          printf("Erreur dans l'allocation mémoire\n");\
          MPI_Abort(communicator, -8); \
     }
