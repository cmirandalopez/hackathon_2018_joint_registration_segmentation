#!/bin/bash

# Slurm submission script, KNL job
# CRIHAN v 1.00 - March 2017 
# support@criann.fr

#SBATCH --reservation hak_knl

# Not shared resources
#SBATCH --exclusive

# Job name
#SBATCH -J "Hybrid_KNL_strict_aliasing"

# Batch output file
#SBATCH --output knl.o%J

# Batch error file
#SBATCH --error knl.e%J

# Partition (submission class)
#SBATCH --partition knl 

# KNL clustering/memory modes
#SBATCH --constraint quad,cache
####SBATCH --constraint quad,flat

# Job time (hh:mm:ss)
#SBATCH --time 1:00:00

# ----------------------------
# Compute nodes number
#SBATCH --nodes 1

# MPI tasks per compute node
#SBATCH --ntasks-per-node 60 

# OpenMP threads per MPI task
#SBATCH --cpus-per-task 2

# Memory per compute node (MB) 
#SBATCH --mem 89000 
# ----------------------------

#SBATCH --mail-type ALL
# User e-mail address
####SBATCH --mail-user firstname.name@address.fr

# Cache memory node
#runcmd=srun

# Flat memory node
runcmd="srun numactl --membind=1"

# Compiler / MPI environments
# ------------------------------
module purge >& /dev/null
module load compilers/intel/2017
module load mpi/intelmpi/2017
# ------------------------------

EXE=TV_joint_V2

LOG=mpicode.${SLURM_NTASKS}
INPFILES="INPUTS/Template01_img2_larger.bin INPUTS/Reference01_img1_larger.bin INPUTS/edge_detector.bin"

cp $EXE $INPFILES $LOCAL_WORK_DIR

cd $LOCAL_WORK_DIR
echo Working directory : $PWD

# MPI profiling
export I_MPI_STATS=ipm
export I_MPI_STATS_FILE=mpistats.out.${SLURM_NTASKS}

# MPI code execution
$runcmd ./$EXE | tee $LOG 

# Copy back output files
# ------------------------------
OUTDIR=$SLURM_SUBMIT_DIR/KNL_OUTPUTS/${SLURM_JOB_NAME}/${SLURM_JOB_ID}_${SLURM_NTASKS}tasks_${OMP_NUM_THREADS}threads
mkdir -p $OUTDIR

mv $EXE $I_MPI_STATS_FILE $LOG final_interpolant_V1.bin $OUTDIR
if test -f gmon.out; then mv gmon.out $OUTDIR; fi
# ------------------------------

