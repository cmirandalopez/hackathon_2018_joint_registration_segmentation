#include "functions.h"
#include "dimens.h"

 void calcul_V (int *tab_bounds, double * V11_local_mat_new, double * V22_local_mat_new, double * V12_local_mat_new, double * V21_local_mat_new, double * U1_local_mat, 
                double *U2_local_mat, double *T_tilde_local, 
                double * V11_local_mat, double * V22_local_mat, double * V12_local_mat, double * V21_local_mat,  
                double * R_local_mat, double coeff_lame_mu, double alpha_q, double c01, double dt, double a2, double rho, double epsilon2, double eps) 
 {

       for(int i=1;i<(tab_bounds[1]-tab_bounds[0]+2);i++){ // parce qu'il y a des cellules fantomes
              for(int j=1;j<(tab_bounds[3]-tab_bounds[2]+2);j++){ // parce qu'il y a des cellules fantomes

                        double detV_2=(1.0+V11_local_mat(i, j))*(1.0+V22_local_mat(i, j))-V12_local_mat(i, j)*V21_local_mat(i, j)-2.0;
                        double coeff2=(1.0+V11_local_mat(i, j))*(1.0+V11_local_mat(i, j))+V12_local_mat(i, j)*V12_local_mat(i, j)+V21_local_mat(i, j)*V21_local_mat(i, j)+(1.0+V22_local_mat(i, j))*(1.0+V22_local_mat(i, j))-alpha_q;
                        double H=heaviside(coeff2,eps);
                        double dir=dirac(coeff2,eps);
                        double coeff4=a2*coeff2*coeff2;


                        V11_local_mat_new(i, j) = (1.0/c01)*(V11_local_mat(i, j)+dt*(-coeff_lame_mu*detV_2*(1.0+V22_local_mat(i, j))
                                                  -4.0*a2*(1.0+V11_local_mat(i, j))*coeff2*H
                                                  -2.0*dir*coeff4*(1.0+V11_local_mat(i, j))+0.5*epsilon2*(U1_local_mat(i, j+1)-U1_local_mat(i, j-1))));


                        V12_local_mat_new(i, j)=(1.0/c01)*(V12_local_mat(i, j)+dt*(coeff_lame_mu*detV_2*V21_local_mat(i, j)
                                                                -4.0*a2*V12_local_mat(i, j)*coeff2*H
                                                                -2.0*dir*coeff4*V12_local_mat(i, j)+0.5*epsilon2*(U1_local_mat(i+1, j)-U1_local_mat(i-1, j))));

                        V21_local_mat_new(i, j)=(1.0/c01)*(V21_local_mat(i, j)+dt*(coeff_lame_mu*detV_2*V12_local_mat(i, j)
                                                                -4.0*a2*V21_local_mat(i, j)*coeff2*H
                                                                -2.0*dir*coeff4*V21_local_mat(i, j)+0.5*epsilon2*(U2_local_mat(i, j+1)-U2_local_mat(i, j-1))));

                        V22_local_mat_new(i, j)=(1.0/c01)*(V22_local_mat(i, j)+dt*(-coeff_lame_mu*detV_2*(1.0+V11_local_mat(i, j))
                                                                -4.0*a2*(1.0+V22_local_mat(i, j))*coeff2*H
                                                                -2.0*dir*coeff4*(1.0+V22_local_mat(i, j))+0.5*epsilon2*(U2_local_mat(i+1, j)-U2_local_mat(i-1, j))));

                }
      }

 }

// ----

void calcul_U (int *tab_bounds, int* voisin, int* voisin_diagonale, MPI_Comm comm2d, int n_max, double theta, double lambdanl, double *U1_local_mat_new, 
               double *U2_local_mat_new, double *U1_local_mat,
               double *U2_local_mat, double *R_local_mat, double *V11_local_mat, double *V12_local_mat, double *V21_local_mat, double *V22_local_mat, 
	       double *dx_interpolant_local, double *dy_interpolant_local, double * interpolant_local, double *f_local, double *T_tilde_local, double dt, 
               double epsilon2, double c0_U1)
{
		
                   for (int i_inter=0;i_inter<n_max;i_inter++){
			#pragma omp for 
                        for (int i=1;i<(tab_bounds[1]-tab_bounds[0]+2);i++){// Parce qu'il y a les cellules fantÃ´mes
                                for (int j=1;j<(tab_bounds[3]-tab_bounds[2]+2);j++){// Parce qu'il y a les cellules fantÃ´mes

                                        U1_local_mat_new(i, j)=(1.0/c0_U1)*(U1_local_mat(i, j)+dt*epsilon2*(U1_local_mat(i+1, j)+U1_local_mat(i-1, j)+U1_local_mat(i, j+1)+U1_local_mat(i, j-1))+dt*(-dx_interpolant_local(i, j)*255.0*255.0*(interpolant_local(i, j)-f_local(i, j)-T_tilde_local(i ,j))/theta-lambdanl*dx_interpolant_local(i, j)*255.0*255.0*(interpolant_local(i, j)-R_local_mat(i, j))-epsilon2*0.5*(V11_local_mat(i, j+1)-V11_local_mat(i, j-1))-epsilon2*0.5*(V12_local_mat(i+1, j)-V12_local_mat(i-1, j))));

                                        U2_local_mat_new(i, j)=(1.0/c0_U1)*(U2_local_mat(i, j)+dt*epsilon2*(U2_local_mat(i+1, j)+U2_local_mat(i-1, j)+U2_local_mat(i, j+1)+U2_local_mat(i, j-1))+dt*(-dy_interpolant_local(i, j)*255.0*255.0*(interpolant_local(i, j)-f_local(i, j)-T_tilde_local(i, j))/theta-lambdanl*dy_interpolant_local(i, j)*255.0*255.0*(interpolant_local(i, j)-R_local_mat(i, j))-epsilon2*0.5*(V21_local_mat(i, j+1)-V21_local_mat(i, j-1))-epsilon2*0.5*(V22_local_mat(i+1, j)-V22_local_mat(i-1, j))));
                                }
                        }

			/*double *temp;
			temp = U1_local_mat_new;
			U1_local_mat_new = U1_local_mat;
			U1_local_mat = temp;
			temp = U2_local_mat_new;
			U2_local_mat_new = U2_local_mat;
			U2_local_mat = temp;*/
			
			
			#pragma omp for 
                        for (int i=1;i<(tab_bounds[1]-tab_bounds[0]+2);i++){// Parce qu'il y a les cellules fantÃ´mes
                                        for (int j=1;j<(tab_bounds[3]-tab_bounds[2]+2);j++){// Parce qu'il y a les cellules fantÃ´mes

                                                U1_local_mat(i, j)=U1_local_mat_new(i, j);
                                                U2_local_mat(i, j)=U2_local_mat_new(i, j);
                                        }
                        }
                        #pragma omp single
			{
                         communication(U1_local_mat,U2_local_mat,tab_bounds,comm2d,voisin,voisin_diagonale);
        		}
		}// fin de la boucle en i_inter

}

