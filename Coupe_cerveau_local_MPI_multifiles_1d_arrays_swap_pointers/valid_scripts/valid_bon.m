fid1=fopen('/home/2018901/HACKATHON_PARTAGE/BROADWELL_OUTPUTS/AsisCode_PureMPI_gprof_500/491849_56tasks_1threads/final_interpolant_V1.bin','r','l'); # ref
fid2=fopen('KNL_OUTPUTS/Hybrid_KNL_strict_aliasing/492606_60tasks_2threads/final_interpolant_V1.bin','r','l');
%fid2=fopen('KNL_OUTPUTS/Hybrid_KNL_strict_aliasing/492566_60tasks_2threads/final_interpolant_V1.bin','r','l');

mydata1=fread(fid1,'double');
mydata2=fread(fid2,'double');

NLIN=1179
NCOL=959



localnorm=abs(mydata1-mydata2);
mynorm=max(localnorm)
%mynorm = 0;

%for (i = 1:NLIN)
%    for(j = 1:NCOL)
%       k = (i-1)*NCOL + j;
%       localnorm = abs(mydata1(k) - mydata2(k))
%       mynorm = max( mynorm, localnorm );
%       #printf('%e\n', localnorm);
%    end
%end

%printf('%e\n', mynorm);
